Cyber Griffin
=============
Side Scrolling Shoot-em Up Game
* Offical Website of The Cyber Griffin (https://filmsbykris.com/games/2021/cyber-griffin/website/)

Credits
=======
# Game Design and Programming
* [Kris Occhipinti] (http://filmsbykris.com)

Game Play
========
* [First 5 Levels] (https://odysee.com/Cyber-Griffin_gameplay_2021.02.16:bc546b3daadebd37ecd60085b49bf1fb6f05763c?src=embed)
* [First look at Death Match] (https://odysee.com/Cyber-Griffin-Game-Play---Death-Match-Mode-2021-02-23-14-52:584e7a78ecb58a75ef8d0f7672f909e5fb38745d?src=embed)

License & Copyrights
====================
* Cyber-Griffin Source Code
Copyright 2021
©Kristofer Occhipinti 
Cyber-Griffin's source code is under the GPLv3 
* License: GPLv3 (see [LICENSE](https://gitlab.com/metalx1000/space-attack-2/-/blob/master/LICENSE) for more information)

Assets [FreeDoom]
=========
Most of the assets for this game (Art, Sounds, Music, etc.) are part of the Freedoom project.
https://freedoom.github.io

Freedoom contains hundreds of original textures, sound effects and music tracks that can be reused royalty-free by Doom level authors and other independent game developers.

Freedoom is liberally licensed under the BSD license - all that is required is that you include a short copyright statement that credits the Freedoom project.
*see CREDITS.txt for list of contributors.
