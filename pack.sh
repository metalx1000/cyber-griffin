#!/bin/bash
###################################################################### 
#Copyright (C) 2021  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

pwd=$PWD
tmp_dir="/tmp/fbk-cyber-griffin"

echo "cleaning..."
rm -fr "$tmp_dir"

function error(){
  echo "There was an error."
  echo $*
  exit 1
}

mkdir -p "$tmp_dir" || error making $tmp_dir
cd src || error entering src
godot3-server --export-pack "pck" "$tmp_dir/cyber-griffin.pck"
cd "$pwd"

find $tmp_dir
