extends Node2D

export (float, 0.1,0.5) var zoom_offset : float = 0.5
export var debug_mode : bool = false

export (float) var zoom_m = 1

var zoom_active = false


var camera_rect : = Rect2()
var viewport_rect : = Rect2()
var active = true
var between = Vector2()
onready var players = get_tree().get_nodes_in_group("players")
onready var animation = $AnimationPlayer
onready var camera = $camera

func _ready():
	Global.camera = self
	viewport_rect = get_viewport_rect()
	camera.zoom.x = zoom_offset
	camera.zoom.y = zoom_offset
	#players[0].remove_child(players[0].camera)
	#players[1].remove_child(players[1].camera)
	#set_process(get_child_count() > 0)
	
func _process(delta: float) -> void:
	if !active:
		return
	
	var player_distance_x = abs(players[0].position.x - players[1].position.x)
	var player_distance_y = abs(players[0].position.y - players[1].position.y)

	if !zoom_active:
		if player_distance_x > 600 || player_distance_y > 500:
			zoom_active = true
			animation.play("zoom_out")
	elif zoom_active:
		if player_distance_x < 500 && player_distance_y < 400:
			zoom_active = false
			animation.play_backwards("zoom_out")
		
	var x = (players[0].position.x + players[1].position.x)/2
	var y = (players[0].position.y + players[1].position.y)/2
	position = Vector2(x,y)
		

func _draw() -> void:
	if not debug_mode:
		return
	#draw_rect(camera_rect, Color("#ffffff"),false)
	draw_circle(between,5,Color("#ffffff"))

