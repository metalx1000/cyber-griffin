extends KinematicBody2D

var touch_damage = 2
var ready = 0
var dead = false
var hit = false
export var life = 50

onready var ray = get_node("RayCast2D")
onready var attacker = get_tree().get_nodes_in_group("players")[0]
onready var weapon = get_node("weapon")

func _ready():
	life = life * Global.difficulty
	if get_node("player"):
		get_node("player").play("fly")
		get_node("player").get_animation("fly").set_loop(true)

func _physics_process(delta):
	if ready < 1:
		ray_detect()
	else:
		ready -= delta + (Global.difficulty * .01)

func _on_Area2D_body_entered(body):
	body.take_damage(self,touch_damage)

func ray_detect():
	if ray.is_colliding():
		ready = 2
		var pos = ray.get_collision_point()
		var body = ray.get_collider()
		Global.create_projectile(self,weapon.global_position,-1,"res://objects/laser.tscn")

func take_damage(creator,damage):
	if !dead:
		life -= damage
		hit = true
		attacker = creator
		if life <= 0:
			death()

func death():
	queue_free()
