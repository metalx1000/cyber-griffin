extends Node2D

var input_count = 1
onready var msg = get_node("msg")
func _ready():
	msg.visible = false
	pass

func _input(event):
	if event is InputEventScreenTouch && input_count < 1:
		input_count -= 1
		get_tree().change_scene("res://UI/controller_message.tscn")
		print("touch")
		#msg.visible = true
		#yield(get_tree().create_timer(3),"timeout")
		#msg.visible = false
		
func _on_exit_released():
	get_tree().change_scene("res://UI/Main_Menu.tscn")
