extends Control
var id = 0
onready var health_bar = get_node("ProgressBar")
func _ready():
	visible = true
	pass
	
func _process(delta):
	#for player in get_tree().get_nodes_in_group("players"):
	var player = get_parent()
#		var id = player.player_id
	var label = get_node("Label")
	var health = "Health: " + str(player.player.health) + "\n"
	var weapon = str(PlayerSettings.weapons[id][PlayerSettings.data[id].weapon_num].weapon.replace("_"," "))
	var ammo = weapon + ": " + str(player.get_ammo(player.weapons[player.player.weapon_num].ammo).amount)
	#var armor = "\nArmor: " + str(player.player.armor) + "\n"
	label.text = health + ammo 
	
	health_bar.value = player.player.health
