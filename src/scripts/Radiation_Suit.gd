extends KinematicBody2D


export (int) var gravity = 1
var velocity = Vector2.ZERO

func _physics_process(delta):
	velocity.y += gravity * Global.gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func _on_Area2D_body_entered(body):
	if body != self && body.is_in_group("players"):
		Global.send_message("Radiation Suit Pickup")
		Sounds.play_sound(body,"bonus")
		body.radiation_suit_time = OS.get_unix_time() + 45
		body.radiation_suit = true
		Global.HUD.color_box.visible = true
		queue_free()
