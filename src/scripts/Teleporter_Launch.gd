extends AnimatedSprite

export var id = 1
export (bool) var sound = true
export (bool) var player_only = false

func _ready():
	visible = false
	pass

func teleport_player(body):
#stop object for a moment
	body.teleporting = true	
	yield(get_tree().create_timer(.5),"timeout")
	body.teleporting = false
	
func teleport_object(body):
#stop object for a moment
	var speed = body.speed
	var gravity
	body.speed = 0
	
	if body.get("gravity") != null:
		gravity = body.gravity
		body.gravity = 0
		
	yield(get_tree().create_timer(.5),"timeout")
	if is_instance_valid(body):
		if body.get("gravity") != null:
			body.gravity = gravity
		body.speed = speed
		if body.get("time_out") != null:
			body.time_out = 0


func _on_Area2D_body_entered(body):
	if player_only && body.is_in_group("enemies"):
		return
		
	if sound:
		Sounds.play_sound(self,"teleporter")
	visible = true
	frame = 0
	play("default")
	
	#find landing pad
	var landing_pads = get_tree().get_nodes_in_group("teleporter_land")
	#randomize search order
	landing_pads = Global.shuffleList(landing_pads)
	for pad in landing_pads:
		if pad.id == id:
			body.global_position = pad.global_position
			pad.activate()
			
			if body.is_in_group("players"):
				teleport_player(body)
			else:
				teleport_object(body)
			break
