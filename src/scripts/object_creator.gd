extends Node2D

export (int) var id = 0
export (bool) var active = false
export (bool) var active_offscreen = false
export (String,FILE,"*.tscn") var object
export var start_delay = 0.0
export var loop_delay = 1.0
export (int) var count = 1
export (bool) var animate = true
export (String) var sound = "teleporter"
export (int) var obj_direction = 0
export var speed = 0
export var speed_up = 0

var onscreen = false
var last_drop = 0.0
var started = false

func _ready():
	pass

func _process(delta):
	if active_offscreen:
		onscreen = true
		
	if active && onscreen:
		start_delay -= delta
		if start_delay <= 0 && count > 0:
			if !started:
				started = true
				drop_object(delta)
			else:
				loop(delta)

func loop(delta):
	last_drop += delta
	if last_drop >= loop_delay:
		last_drop = 0.0
		drop_object(delta)

func animation():
	if animate:
		$Sprite.frame = 0
		$Sprite.play("default")
		$Sprite.visible = true

func drop_object(delta):
	animation()
	if sound != "":
		Sounds.play_sound(self,sound)
		
	if count > 0:
		count -= 1
		var obj = load(object).instance()
		add_child(obj)
		if !obj.get("creator") == null:
			obj.creator = self
		
		if !obj.get("active") == null:
			obj.active = true
			obj.attacker = Global.players[0]

		if !obj.get("speed") == null:
			obj.speed = speed
		if !obj.get("speed_up") == null:
			obj.speed_up = speed_up
			
		if obj_direction != 0:
			obj.direction = obj_direction
						
		

func _on_VisibilityNotifier2D_screen_entered():
	onscreen = true
	pass # Replace with function body.


func _on_VisibilityNotifier2D_screen_exited():
	onscreen = false
