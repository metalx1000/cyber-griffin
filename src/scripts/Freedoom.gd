extends Control

onready var menu = $CanvasLayer/Menu

func _ready():
	menu.visible = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	pass # Replace with function body.


func _on_website():
	OS.shell_open("https://freedoom.github.io/")

func _input(event):
	if event.is_action_pressed("menu_about"):
		toggle_pause()
		
func toggle_pause():
	var pause_state = !get_tree().paused
	menu.visible = pause_state
	if !get_tree().paused:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = pause_state
		

func _on_exit():
	get_tree().change_scene("res://UI/Main_Menu.tscn")
	pass # Replace with function body.
