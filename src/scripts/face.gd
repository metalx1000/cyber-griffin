extends AnimatedSprite

onready var player = get_tree().get_nodes_in_group("players")[0]
var last_time = OS.get_unix_time()
var player_health

func _ready():
	player_health = PlayerSettings.data[0].health
	Global.face = self
	play("80")


func _process(delta):
	player = get_tree().get_nodes_in_group("players")[0]
	if player == null:
		return
	player_health = PlayerSettings.data[0].health
	happy()
	dead()
	hit()
	
	if OS.get_unix_time() - last_time > 2:
		last_time = OS.get_unix_time()
		
		if player.godmode:
			play("god")
		elif player_health > 80:
			play("80")
		elif player_health > 60:
			play("60")
		elif player_health > 40:
			play("40")
		elif player_health > 20:
			play("20")
		elif player_health > 0:
			play("00")
			
func dead():
	if player.dead:
		play("dead")

func hit():
	if player.shocked:
		if player.godmode:
			play("god")
		elif player_health > 80:
			play("hit_80")
		elif player_health > 60:
			play("hit_60")
		elif player_health > 40:
			play("hit_40")
		elif player_health > 20:
			play("hit_20")
		elif player_health > 0:
			play("hit_00")
			
func happy():
	if player.happy:
		if player.godmode:
			play("god")
		elif player_health > 80:
			play("happy_80")
		elif player_health > 60:
			play("happy_60")
		elif player_health > 40:
			play("happy_40")
		elif player_health > 20:
			play("happy_20")
		elif player_health > 0:
			play("happy_00")
