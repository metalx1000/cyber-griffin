extends KinematicBody2D
export (float) var gravity = 1

var velocity = Vector2.ZERO
export (String) var snd = "bonus"
export (int) var value = 25
export (int) var max_value = 100
export (String) var message = ""
export var type = ""
var time_out = 0

var onscreen = false

func _physics_process(delta):
	if onscreen:
		time_out += delta
		if time_out < 2 || onscreen:
			velocity.y += gravity * Global.gravity * delta
			velocity = move_and_slide(velocity, Vector2.UP)
	
func pickup(body):
	if body != self && body.is_in_group("players"):
		if body.player.health < max_value:
			Global.send_message(type + " Pickup")
			Sounds.play_sound(body,snd)
			queue_free()
			body.player.health += value
			if body.player.health > max_value:
				body.player.health = max_value
			
			if value > 99:
				body.make_happy()
		
		if type == "Invinciblity":
			body.player.health = 100
			body.invincible_time += 30


func _on_onscreen_screen_entered():
	onscreen = true



func _on_onscreen_screen_exited():
	onscreen = false
	time_out = 0
