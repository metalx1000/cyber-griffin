extends Node

export (String,FILE) var music
export (String) var map_name
export (String) var map_creator

onready var player = get_tree().get_nodes_in_group("players")[0]
#export var intermission_msg = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	startup_message()
	Global.current_level = map_name
	Global.total_secrets = 0
	Global.secrets_found = 0
	Global.multi_player = false
	Global.enemies_killed = 0
	Global.world_bounds = $Map/World.get_used_rect()
	Global.world_cell_size = $Map/World.cell_size
	
	Global.dialog = player.get_node("CanvasLayer/Confirm_Dialog")
	Global.level_time = 0
	Global.HUD.stats_label.visible = false
	clear_player_data()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	load_music()
	add_level_reached()
	
func _input(event):
	if event.is_action_pressed("start_menu"):
		player.get_node("CanvasLayer/Confirm_Dialog/VBoxContainer/HBoxContainer/No").grab_focus()
		get_tree().paused = true
		Global.dialog.visible = true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func startup_message():
	if Global.startup_msg != "":
		Global.send_message2(Global.startup_msg)
		Global.startup_msg = ""
		yield(get_tree().create_timer(2),"timeout")
	
	if map_creator != "":
		Global.send_message("Map by: " + map_creator)
	Global.send_message2(map_name)

func clear_player_data():
	PlayerSettings.keys = [[],[]]

func add_level_reached():
	var level = get_tree().current_scene.filename
	if !Global.check_points.has(level):
		Global.check_points.append(level)
		Global.save_settings("maps","check_points",Global.check_points)


func load_music():
	#check if file exists, if not play random track
	if ResourceLoader.exists(music):
		Music.play_resource(music)
	else:
		Music.random_track()
