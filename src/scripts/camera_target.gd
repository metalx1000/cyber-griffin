extends Node2D

onready var players = get_tree().get_nodes_in_group("players")
onready var player_1 = players[0]
onready var player_2 = players[1]

onready var tween = Tween.new()
onready var move_tween = Tween.new()

var new_pos = Vector2()
var move_time = 0.0
var zoom_time = 0.0
var zoom_delay = 0.5

func _ready():
	add_child(tween)
	add_child(move_tween)
	move_camera()
	#set_camera_bounds()
	zoom()
	tween.connect("tween_completed", self, "_on_zoom_completed")
	move_tween.connect("tween_completed", self, "_on_tween_completed")
	remove_player_cameras()
	pass

		
func calc_pos():
	var p1_y = player_1.global_position.y
	var p1_x = player_1.global_position.x
	var p2_y = player_2.global_position.y
	var p2_x = player_2.global_position.x
	var pos_x
	var pos_y
	
	if p1_y > p2_y:
		pos_y = (p1_y + p2_y)/2
	else:
		pos_y = (p2_y + p1_y)/2
		
	if p1_x > p2_x:
		pos_x = (p2_x + p1_x)/2
	else:
		pos_x = (p1_x + p2_x)/2

	var pos = Vector2(pos_x,pos_y)

	return pos 
	
func remove_player_cameras():
	for p in get_tree().get_nodes_in_group("players"):
		var ps = p.camera.global_position
		p.remove_child(p.camera)
		
		
func move_camera():
	new_pos = calc_pos()
	#move_tween.stop_all()
	move_tween.interpolate_property($camera,\
		"global_position",\
		 $camera.global_position,\
		 new_pos, zoom_delay, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	move_tween.start()
	#$camera.global_position = lerp($camera.global_position, new_pos, .05)
	#$camera.global_position = new_pos
	
func set_camera_bounds():
	yield(get_tree().create_timer(.2),"timeout")
	if Global.world_bounds != null:
		var world_pos = Global.world_bounds.position
		var world_end = Global.world_bounds.end
		var cell_size = Global.world_cell_size
		if $camera != null:
			$camera.limit_left = world_pos.x * cell_size.x
			$camera.limit_right = world_end.x * cell_size.x
			$camera.limit_top = world_pos.y * cell_size.y
			$camera.limit_bottom = world_end.y * cell_size.y
	
	
func zoom():
	var distance = player_1.global_position.distance_to(player_2.global_position)
	var zoom = distance/700
	if zoom < .5:
		zoom = .5
		
	tween.interpolate_property($camera, "zoom",\
		 $camera.zoom, Vector2(zoom, zoom),\
		 zoom_delay, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	#$camera.zoom = Vector2(zoom,zoom)

func _on_tween_completed(object,key):
	move_camera()

func _on_zoom_completed(object,key):
	zoom()
