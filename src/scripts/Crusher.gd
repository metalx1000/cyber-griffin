extends Node2D
var time = 0
export (int) var id = 0
export (float) var speed = 1
export (bool) var sound = true
export (int) var damage = 1
var position_num = 0

var dead = false
var onscreen = false
onready var animation = $AnimationPlayer
onready var snd = $Crusher/sound
onready var collider = $Crusher/Area2D/CollisionShape2D

func _ready():
	animation.stop()
	animation.playback_speed = speed
	damage = damage * Global.difficulty
	if sound:
		snd.play()

func _process(delta):
	if onscreen:
		if time < .5:
			time += delta
			collider.disabled = false
		else:
			time = 0
			collider.disabled = true
		
func _on_Area2D_body_entered(body):
	body.take_damage(self,damage)

func _on_onscreen_screen_entered():
	onscreen = true
	animation.play("move")

func _on_onscreen_screen_exited():
	onscreen = false
	animation.stop()
