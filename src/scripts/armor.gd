extends KinematicBody2D

var gravity = 1

var velocity = Vector2.ZERO
export (int) var value = 100
export (int) var max_value = 100
export (String) var type = "Armor"

func _physics_process(delta):
	velocity.y += gravity * Global.gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	
func pickup(body):
	if body != self && body.is_in_group("players"):
		if body.player.armor < max_value:
			Global.send_message(type + " Pickup")
			Sounds.play_sound(body,"bonus")
			queue_free()
			body.player.armor += value
			if body.player.armor > max_value:
				body.player.armor = max_value
