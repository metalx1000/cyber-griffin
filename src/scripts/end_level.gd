extends AnimatedSprite
export (String,FILE) var next_scene

func _ready():
	visible = false
	pass

func next():
	if next_scene == "":
		Global.next_scene = "res://UI/Main_Menu.tscn"
	else:
		Global.next_scene = next_scene
		get_tree().change_scene("res://UI/stats_screen.tscn")

func _on_Area2D_body_entered(body):
	Sounds.play_sound(self,"teleporter")
	visible = true
	frame = 0
	play("default")
	
	get_tree().paused = true
	
	yield(get_tree().create_timer(2),"timeout")
	get_tree().paused = false
	Global.players.clear()
	next()
