extends Node2D
onready var player = get_node("player")
onready var elevator = get_node("ParallaxBackground")
onready var ani = get_node("AnimationPlayer")
onready var red_flash = get_node("red_flash")
onready var alarm = get_node("alarm")
var clicks = 0

var play_alarm = false

export (float) var speed = 100
onready var time = 0.0

func _ready():
	Music.fade_out()
	ani.play("default")
	ani.seek(0)
	pass

func _process(delta):
	time += delta
	if time > 65:
		next_scene()
	elif time > 50:
		red_flash.stop()
		alarm.volume_db = lerp(alarm.volume_db,-80,.01)
	elif time > 30:
		if !play_alarm:
			play_alarm = true
			alarm.play()
		red_flash.get_animation("red").set_loop(true)
		red_flash.play("red")
		
	player.position.y -= speed * delta

func _input(event):
	if event is InputEventJoypadButton || \
	event is InputEventKey || \
	event is InputEventMouseButton:
		if clicks == 0:
			Sounds.play_sound(null,"shotgun")
		clicks += 1
		if clicks == 3:
			Sounds.play_sound(null,"shotgun")
			ani.seek(57)	
			yield(get_tree().create_timer(4),"timeout")
			next_scene()
			
func next_scene():
	get_tree().change_scene("res://UI/Main_Menu.tscn")
