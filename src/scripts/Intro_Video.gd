extends Node2D

onready var player = get_node("VideoPlayer")
onready var fader = get_node("AnimationPlayer")

func _ready():
	fader.seek(0)
	player.modulate = Color(1,1,1,1)
	player.volume_db = 0.0
	player.connect("finished",self,"next_scene")
	add_child(player)
	player.set_size(get_viewport_rect().size)
	var video = load("res://res/videos/cyber_intro_1c_s.ogv")
	player.stream = video
	player.play()
	return player
	
func _input(event):
	fader.play("fade")
	if Input.is_action_just_pressed("shoot") || event is InputEventScreenTouch:
		fader.play("fade")

func next_scene():
	get_tree().change_scene("res://UI/Main_Menu.tscn")
