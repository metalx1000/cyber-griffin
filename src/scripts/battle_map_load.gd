extends Node

export (String,FILE) var music
export (String) var map_name
export (String) var map_creator

var camera
#export var intermission_msg = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	load_camera()
	
	startup_message()
	Global.current_level = map_name
	Global.difficulty = 2
	Global.total_secrets = 0
	Global.secrets_found = 0
	Global.enemies_killed = 0
	Global.world_bounds = $Map/World.get_used_rect()
	Global.world_cell_size = $Map/World.cell_size
	Global.multi_player = true
	Global.dialog = $Player/CanvasLayer/Confirm_Dialog
	Global.level_time = 0
	clear_player_data()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	load_music()
	#load_touch_controls()
	#add_level_reached()
	
func load_touch_controls():
	var controls = load("res://UI/Battle_Map_Controls.tscn").instance()
	add_child(controls)

	
func _input(event):
	camera.add_new_player(event)
	if event.is_action_pressed("start_menu"):
		get_tree().change_scene("res://UI/Main_Menu.tscn")
		#$Player/CanvasLayer/Confirm_Dialog/VBoxContainer/HBoxContainer/No.grab_focus()
		#get_tree().paused = true
		#Global.dialog.visible = true
		#Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if event is InputEventScreenTouch:
		get_tree().change_scene("res://UI/controller_message.tscn")


	
func startup_message():
	if Global.startup_msg != "":
		Global.send_message2(Global.startup_msg)
		Global.startup_msg = ""
		yield(get_tree().create_timer(2),"timeout")
	
	if map_creator != "":
		Global.send_message("Map by: " + map_creator)
	Global.send_message2(map_name)

func clear_player_data():
	PlayerSettings.keys = [[],[]]

func load_camera():
	camera = preload("res://objects/camera_multiplayer.tscn").instance()
	add_child(camera)

func load_music():
	#check if file exists, if not play random track
	if ResourceLoader.exists(music):
		Music.play_resource(music)
	else:
		Music.random_track()
