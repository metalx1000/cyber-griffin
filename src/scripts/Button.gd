extends Button

#export var reference_path = ""
export (String,FILE) var reference_path
export(bool) var start_focused = false
export(bool) var cancel = false
export var url = ""
export (String) var function = null
export (int) var difficulty = 0

func _ready():
	if(start_focused):
		grab_focus()
	connect("mouse_entered",self,"_on_Button_mouse_entered")
	connect("focus_entered",self,"_on_Button_mouse_entered")
	connect("pressed",self,"_on_Button_Pressed")

func _on_Button_mouse_entered():
	Sounds.play_sound(null,"menu")
	grab_focus()
	
func set_difficulty():
	if difficulty > 0:
		Global.difficulty = difficulty
	
	if difficulty > 4:
		Global.startup_msg = "You're Crazy"

func _on_Button_Pressed():
	get_tree().paused = false

	if is_instance_valid(Global.dialog):
		Global.dialog.visible = false
	
	Sounds.play_sound(null,"menu_select")
		
	
	if cancel:
		return
	#yield(get_tree().create_timer(.5),"timeout")
	set_difficulty()	
	if url != "":
		OS.shell_open(url)
		return
	
	if(reference_path != ""):
		get_tree().change_scene(reference_path)
	else:
		get_tree().quit()
