extends Control

onready var menu = $CanvasLayer/Menu

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _input(event):

	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://UI/Main_Menu.tscn")


func _on_key_released():
	Cheatcodes.unlock_levels()
	pass # Replace with function body.
