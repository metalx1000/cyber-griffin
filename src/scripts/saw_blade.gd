extends Node2D
export var speed = 20

export (float) var movement_x = 0
export (float) var movement_y = 0
export (bool) var sound = true
export (int) var damage = 2

onready var saw_blade = $saw_blade
onready var animation = $AnimationPlayer
var position_num = 0

var dead = false
var onscreen = false
onready var tween = Tween.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	animation.playback_speed = rand_range(.8,1.2)
	if movement_y != 0:
		animation.play("up_down")
		
	damage = damage * (Global.difficulty/2)
	if sound:
		$sound.play()
	
func _process(delta):
	saw_blade.rotation += speed * delta
		
func _on_Area2D_body_entered(body):
	body.take_damage(self,damage)


