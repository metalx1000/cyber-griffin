extends Node

var speed = 300
var jump_speed = -550
var gravity = 1

var default_data = {"health": 100, 
	"armor" : 0,
	"gravity" : 1,
	"speed" : 20000,
	"jump_speed" : -500,
	"weapon_num": 0,
	"heal" : false,
	"secrets":0
	}

var data = [
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true),
		default_data.duplicate(true)
]

var keys = [[],[],[],[]]

var default_weapons = [
		{"weapon":"pistol", "ammo":"bullets_p","active":true},
		{"weapon":"shotgun","ammo":"shells","active":false},
		{"weapon":"double_barrel","ammo":"shells","active":false},
		{"weapon":"chaingun","ammo":"bullets_c","active":false},
		{"weapon":"rocket_launcher","ammo":"rockets","active":false},
		{"weapon":"plasma_gun","ammo":"plasma","active":false},
]

var weapons = [
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true),
	default_weapons.duplicate(true)
]

var default_ammo = 	[
		{ "type":"bullets_p", "amount": 10, "max_amount" : 100},
		{ "type":"bullets_c", "amount": 0, "max_amount" : 400},
		{ "type":"shells", "amount": 0, "max_amount" : 50},
		{ "type":"plasma", "amount": 0, "max_amount" : 300},
		{ "type":"rockets", "amount": 0, "max_amount" : 50}
	]
	
var ammo = [
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true),
	default_ammo.duplicate(true)
]


func _ready():
	pass
