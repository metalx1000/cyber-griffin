extends Control

onready var box1 = get_node("Menu/VBoxContainer/HBoxContainer/VBoxContainer")
onready var box2 = get_node("Menu/VBoxContainer/HBoxContainer/VBoxContainer2")
onready var box3 = get_node("Menu/VBoxContainer/HBoxContainer/VBoxContainer3")
onready var box4 = get_node("Menu/VBoxContainer/HBoxContainer/VBoxContainer4")

func _ready():
	var i = 0
	var focus = true
	var check_points = Global.check_points
	check_points.sort_custom(self,"customComparison")
	for level in check_points:
		i += 1
		var button = load("res://UI/Button.tscn").instance()
		var map = level.replace("res://maps/","")
		button.text = map.replace(".tscn","")
		button.reference_path = level
		if focus:
			focus = false
			button.start_focused = true
		var dynamic_font = load("res://res/fonts/Button_Small.tres")
		dynamic_font.size = 128
		button.set("custom_fonts/font", dynamic_font)
		#button.get("custom_fonts/font").set_size(64)
		if i == 1:
			box1.add_child(button)
		elif i == 2:
			box2.add_child(button)
		elif i == 3:
			box3.add_child(button)
		elif i == 4:
			box4.add_child(button)
			i = 0

func customComparison(a, b):
	if typeof(a) != typeof(b):
		return typeof(a) < typeof(b)
	else:
		return a < b

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		Global.play_sound(null,"res://res/sounds/menu_select.ogg")
		get_tree().change_scene("res://UI/Main_Menu.tscn")
