extends KinematicBody2D

export var object = ""
export (bool) var object_add = false
export (int) var amount = 10
export (String,"none","bullets_p","bullets_c","shells","plasma","rockets") var ammo_type

var onscreen = false
var time_out = 0
var gravity = 1
var velocity = Vector2.ZERO

onready var body = get_node("Area2D")

func _ready():
	if object == "rocket":
		set_collision_layer_bit(1,true)
		add_to_group("shootable")

func take_damage(creator,damage):
	if object == "rocket":
		queue_free()
		var rocket = load("res://objects/rock_fire.tscn").instance()
		get_tree().current_scene.add_child(rocket)
		rocket.global_position = global_position
		var death = rocket.get_node("AnimationPlayer")
		death.play("death")
		rocket.death()
		
func _physics_process(delta):
	time_out += delta
	if time_out < 2 || onscreen:
		velocity.y += gravity * Global.gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)


func pickup(body):
	if body != self && body.is_in_group("players"):
		if object_add:
			var i = 0
			for w in body.weapons:
				if w.weapon == object:
					#auto switch weapon if picking up for first time
					if w.active != true:
						body.player.weapon_num = i
					w.active = true
					break
				i += 1

		var ammo = body.get_ammo(ammo_type)
		if ammo.amount < ammo.max_amount:
			Global.send_message(object + " Pickup")
			var snd = Sounds.play_sound(body,"bonus")
			var a = body.get_ammo(ammo_type).amount
			ammo.amount += amount
			if ammo.amount > ammo.max_amount:
				ammo.amount = ammo.max_amount
			queue_free()


func _on_onscreen_screen_entered():
	onscreen = true

func _on_onscreen_screen_exited():
	onscreen = false
	time_out = 0
