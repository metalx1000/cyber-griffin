extends KinematicBody2D

export var move_speed = 5.0
export var move_distance = 500.0
export var move_direction = Vector2(0, 1)
#export var p = false

var origin = Vector2(0, 0)
var bottom = 0
var top = position.y - move_distance
var direction = null


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	origin = global_position
	top = global_position.y - move_distance
	bottom = global_position.y

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#       pass

func _physics_process(delta):
	if direction == "up":
		if global_position.y > top:
			global_position.y -=  move_speed
		else:
			direction == null
	elif direction == "down":
		if global_position.y < bottom:
			global_position.y +=  move_speed
		else:
			direction == null
			

func lift_up(body):
	if body.is_in_group("players"):
		body.on_lift = true
		Sounds.play_sound(self,"door_open")
		direction = "up"

func lift_down(body):
	if body.is_in_group("players"):
		body.on_lift = false
		Sounds.play_sound(self,"door_close")
		direction = "down"


func _on_bottom_body_entered(body):
	#if body != self && body.is_in_group("players"):
	if !body.dead:
		Sounds.play_sound(self,"door_open")
		direction = "up"
		yield(get_tree().create_timer(2),"timeout")
		Sounds.play_sound(self,"door_close")
		direction = "down"
