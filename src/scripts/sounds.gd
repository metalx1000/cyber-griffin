extends Node

#reload sounds
###player Sounds ###
var player_hit = preload("res://res/sounds/player_hit.ogg")
var player_death = preload("res://res/sounds/player_death.ogg")
var player_death_pain = preload("res://res/sounds/player_death_pain.ogg")
###Enemy Sounds###
#pistol guy
var enemy_pos_site_1 = preload("res://res/sounds/enemy_pos_site_1.ogg")
var enemy_pos_site_2 = preload("res://res/sounds/enemy_pos_site_2.ogg")
var enemy_pos_site_3 = preload("res://res/sounds/enemy_pos_site_3.ogg")

var enemy_pos_pain_1 = preload("res://res/sounds/enemy_pos_pain.ogg")
var enemy_pos_death_1 = preload("res://res/sounds/enemy_pos_death_1.ogg")
var enemy_pos_death_2 = preload("res://res/sounds/enemy_pos_death_2.ogg")
var enemy_pos_death_3 = preload("res://res/sounds/enemy_pos_death_3.ogg")

#creature
var enemy_creature_site_1 = preload("res://res/sounds/enemy_creature_site_1.ogg")
var enemy_creature_site_2 = preload("res://res/sounds/enemy_creature_site_2.ogg")
var enemy_creature_death_1 = preload("res://res/sounds/enemy_creature_death_1.ogg")
var enemy_creature_death_2 = preload("res://res/sounds/enemy_creature_death_2.ogg")
var enemy_creature_act = preload("res://res/sounds/enemy_creature_act.ogg")

#pink enemy
var enemy_pink_site = preload("res://res/sounds/pink_site.ogg")
var enemy_pink_bite = preload("res://res/sounds/pink_bite.ogg")
var enemy_pink_death = preload("res://res/sounds/pink_death.ogg")

###Weapons###
var pistol = preload("res://res/sounds/w_pistol.ogg")
var shotgun = preload("res://res/sounds/w_shotgun_1.ogg")
var double_barrel = preload("res://res/sounds/double_barrel_reload.ogg")

var plasma_shot = preload("res://res/sounds/plasma_shot.ogg")
var plasma_death = preload("res://res/sounds/plasma_shot_death.ogg")

var rocket = preload("res://res/sounds/w_rocket_launcher.ogg")
var rocket_death = preload("res://res/sounds/explode_1.ogg")

var fireball = preload("res://res/sounds/fireball_shoot.ogg")
var fireball_death = preload("res://res/sounds/plasma_shot_death.ogg")

var laser = preload("res://res/sounds/laser.ogg")

#misc Sounds
var bonus = preload("res://res/sounds/powerup.ogg")
var teleporter = preload("res://res/sounds/teleport.ogg")
var explode = preload("res://res/sounds/explode_1.ogg")
var door_open = preload("res://res/sounds/door_open_1.ogg")
var door_close = preload("res://res/sounds/door_close_1.ogg")

#UI Sounds
var menu = preload("res://res/sounds/menu.ogg")
var menu_select = preload("res://res/sounds/menu_select.ogg")

func _ready():
	pass # Replace with function body.


func play_sound(creator,snd):
	snd = self.get(snd)
	var stream_player
	if creator == null:
		stream_player = AudioStreamPlayer.new()
		add_child(stream_player)
	else:
		stream_player = AudioStreamPlayer2D.new()
		creator.add_child(stream_player)
	
	stream_player.set_bus("sounds")
	stream_player.stream = snd
	if is_instance_valid(stream_player.stream):
		stream_player.stream.set_loop(false)
	stream_player.connect("finished", stream_player, "queue_free")
	
	#stream_player.volume_db = Global.sound_fx_vol
	
	stream_player.play()
	return stream_player
