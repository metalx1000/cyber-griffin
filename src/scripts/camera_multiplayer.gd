extends Camera2D

export (float, 0.1,0.5) var zoom_offset : float = 0.2
export var debug_mode : bool = false

export (float) var zoom_m = 1

#var camera_rect : = Rect2()
#var viewport_rect : = Rect2()
export var active = false

func _ready():
	Global.camera = get_tree().get_nodes_in_group("multiplayer_camera")[0]
	#viewport_rect = get_viewport_rect()
	#set_process(get_child_count() > 0)
	
func _process(delta: float) -> void:
	if active:
		current = true
		#camera_rect = Rect2(get_child(0).global_position,Vector2())
		for index in get_child_count():
			if index == 0:
				continue
			#camera_rect = camera_rect.expand(get_child(index).global_position)
			
			#var new_offset = calculate_center(camera_rect)
			#offset = lerp(offset,new_offset,0.05)
			#zoom = calculate_zoom(camera_rect,viewport_rect.size)
			#position = new_offset
			#position = new_offset
			
			if debug_mode:
				update()
			
	#else:
	#	if get_child_count() > 0:
	#		active = true

func load_player():
	var player = Global.load_player(0)
	var start_group = get_tree().get_nodes_in_group("player_start")
	var pos = Global.shuffleList(start_group)[0]
	player.global_position = pos.global_position
	pos.active = true
	add_child(player)
	player.remove_child(player.camera)
	var overhead = preload("res://UI/player_overhead.tscn").instance()
	
	player.add_child(overhead)

func add_new_player(event):
	var id = event.get_device()
	
	for player in get_tree().get_nodes_in_group("players"):
		if player.player_id == id:
			return
			
	var player = Global.load_player(id)
	player.remove_child(player.camera)
	var overhead = preload("res://UI/player_overhead.tscn").instance()
	overhead.id = id
	player.add_child(overhead)
	add_child(player)


