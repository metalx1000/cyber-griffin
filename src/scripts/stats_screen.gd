extends Control
var next_scene = Global.next_scene
export (String,FILE) var music

var time_elapse = 0.0
var background
var text
var clicks = 0
var level = Global.current_level


func _ready():
	load_music()
	$text.text = level
	$Continue.reference_path = next_scene
	yield(get_tree().create_timer(1),"timeout")
	load_kills()

		
func load_kills():
	for k in Global.enemies_killed:
		var kills = str(k)+"/"+str(Global.total_enemies)
		$text.text = level + "\n\n"
		$text.text += "Kills                     " + kills
		Sounds.play_sound(null,"pistol")
		yield(get_tree().create_timer(.1),"timeout")
		if clicks > 0:
			break
	yield(get_tree().create_timer(1),"timeout")
	load_secrets()
	

func load_secrets():
	var kills = str(Global.enemies_killed)+"/"+str(Global.total_enemies)
	#if zero secrets then still display
	$text.text = level + "\n\n"
	$text.text += "Kills                     " + kills
	$text.text += "\nSecrets                     " + str(Global.secrets_found)
		
	for s in Global.secrets_found:
		var secrets = str(s+1)+"/"+str(Global.total_secrets)
		$text.text = level + "\n\n"
		$text.text += "Kills                     " + kills
		$text.text += "\nSecrets                     " + secrets
		Sounds.play_sound(null,"pistol")
		yield(get_tree().create_timer(.1),"timeout")
	
	yield(get_tree().create_timer(1),"timeout")
	load_time()
	
func load_time():
	Sounds.play_sound(null,"shotgun")
	var time = int(Global.level_time)
	time = Global.format_time(time)
	$text.text += "\n\nTime: " + str(time)

	
func load_music():
	#check if file exists, if not play random track
	if ResourceLoader.exists(music):
		Music.play_resource(music)
	else:
		Music.random_track()

func next():
	yield(get_tree().create_timer(.2),"timeout")
	if next_scene == "":
		get_tree().change_scene("res://UI/Main_Menu.tscn")
	else:
		get_tree().change_scene(next_scene)

func _input(event):
	if Input.is_action_just_pressed("jump") || Input.is_action_just_pressed("shoot"):
		Sounds.play_sound(null,"shotgun")
		clicks += 1
		if clicks == 2:
			next()
