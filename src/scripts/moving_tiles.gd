extends Node2D

var active = false
export (int) var id = 0
export (float) var speed = 1
export (bool) var reverse = false


func _ready():
	add_to_group("object_creators")
	$AnimationPlayer.play("activate")
	$AnimationPlayer.stop()
	if reverse:
		$AnimationPlayer.seek(100,true)
	else:
		$AnimationPlayer.seek(0,true)



func _process(delta):
	if active:
		active = false
		$AnimationPlayer.playback_speed = speed
		if reverse:
			$AnimationPlayer.play_backwards("activate")
		else:
			$AnimationPlayer.play("activate")
		reverse = !reverse
