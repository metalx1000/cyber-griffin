extends KinematicBody2D

var opening = false
var closing = false

var closed_pos
var opened_pos
var stop_distance = 1

var active = false

export (int) var speed = 2000
export (int) var door_frame = 0
export(String, "none", "red key","blue key","yellow key","red skull key","blue skull key","yellow skull key") var door_type

export (int) var open_delay = 0
export (int) var close_delay = 3

export (String) var message = null

export (String,FILE,"*.ogg") var open_snd = "res://res/sounds/door_open_1.ogg"
export (String,FILE,"*.ogg") var close_snd = "res://res/sounds/door_close_1.ogg"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.frame = door_frame
	closed_pos = Vector2(position)
	opened_pos = Vector2(closed_pos.x, closed_pos.y - 128)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if opening:
		if position.distance_to(opened_pos) > stop_distance:
			var move_pos = opened_pos - position
			move_pos = move_pos.normalized()
			move_and_slide(move_pos * speed * delta * 4)
			#yield(get_tree().create_timer(close_delay),"timeout")
		else:
			opening = false
			yield(get_tree().create_timer(close_delay),"timeout")
			Global.play_sound(self,close_snd)
			closing = true
			
	elif closing:
		if position.distance_to(closed_pos) > stop_distance:
			var move_pos = closed_pos - position
			move_pos = move_pos.normalized()
			move_and_slide(move_pos * speed * delta * 4)
			#yield(get_tree().create_timer(close_delay+1),"timeout")
		else:
			closing = false
			active = false
			
func keyed(body):
	if door_type != "none":
		if !body.keys.has(door_type):
			if message == null:
				active = false
				Global.send_message2(door_type + " is needed")
			return true
	return false

func open(body):
	if body != self && body.is_in_group("players") && !opening:
		if active:
			return
		else:
			active = true
		
		if if is_instance_valid(message):
			Global.send_message2(message)
		
		if keyed(body):
			return
		
		if !opening:
			yield(get_tree().create_timer(open_delay),"timeout")
			opening = true
			Global.play_sound(self,open_snd)
		
