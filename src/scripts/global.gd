extends Node

var score = 0

var config = ConfigFile.new()
var settings_file = "user://settings.cfg"

#repload weapons
var plPuff = preload("res://objects/puff.tscn")
var bullets = preload("res://objects/bullets.tscn")
var shells = preload("res://objects/shells.tscn")
var plasma_shots = preload("res://objects/plasma_shot.tscn")
var fireballs = preload("res://objects/fireball.tscn")
var headfire = preload("res://objects/head_fire.tscn")
var rockets = preload("res://objects/rock_fire.tscn")
var shotguns = preload("res://objects/shotgun.tscn")

var HUD
var face
var players = []
var hidden_tiles = []
var check_points = ["res://maps/e1m1.tscn"]

var world_bounds
var world_cell_size
var dialog
var multi_player = false

var console = "Console:"

var next_scene = "res://UI/Main_Menu.tscn"
var total_enemies = 0
var enemies_killed = 0
var current_level = ""

var gravity = 1000
var sound_fx_vol = 0

var total_secrets = 0
var secrets_found = 0
var level_time = 0

var camera
var difficulty = 2.0
var startup_msg = ""

var number_of_players = 3

var scene_id = 0
var scenes = [
	"res://maps/e1m1.tscn",
	"res://UI/Intermission_1.tscn",
	"res://maps/e1m2.tscn",
]

#number of message for intermission
var msg_num = 0

var key_types = ["redkey","bluekey","yellowkey","redskull","blueskull","yellowskull"]

func _ready():
	randomize() 
	load_settings()

func _input(event):
	if Input.is_action_pressed("reload"):
		Input.action_release("reload")
		reload_map()
		
	#music Volume adjustments
	var music_vol = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("music"))
	if Input.is_action_pressed("volume_up"):
		if  music_vol < 2:
			music_vol += 1
			get_volume_percent(music_vol, "music")
			save_settings("audio","music_volume",music_vol)
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music"),music_vol)
	if Input.is_action_pressed("volume_down"):
		if music_vol > -80:
			music_vol -= 1
			get_volume_percent(music_vol, "music")
			save_settings("audio","music_volume",music_vol)
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music"),music_vol)
			
	if event.is_action_pressed("toggle_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen

func get_volume_percent(db,type):
	#based on -80db to 2db
	var p = (db + 80) * 1.21
	p = int(p)
	if p == 99:
		p = 100
	Global.send_message(type + " Volume: " + str(p))

func play_sound(creator,res):
	if  ResourceLoader.exists(res):
		var stream_player
		if creator == null:
			stream_player = AudioStreamPlayer.new()
			add_child(stream_player)
		else:
			stream_player = AudioStreamPlayer2D.new()
			creator.add_child(stream_player)
		stream_player.stream = ResourceLoader.load(res)
		stream_player.stream.set_loop(false)
		stream_player.connect("finished", stream_player, "queue_free")
		
		#stream_player.volume_db = sound_fx_vol
		stream_player.play()
		return stream_player

func create_puff(pos,creator):
	var puff = plPuff.instance()
	puff.creator = creator
	puff.position = pos
	get_tree().current_scene.add_child(puff)

func drop_bullets(creator):
	var obj = bullets.instance()
	#obj.creator = creator
	obj.position = creator.position
	get_tree().current_scene.add_child(obj)
	
func drop_shells(creator):
	var obj = shells.instance()
	#obj.creator = creator
	obj.position = creator.position
	get_tree().current_scene.add_child(obj)

func drop_shotgun(creator):
	var obj = shotguns.instance()
	#obj.creator = creator
	obj.position = creator.position
	get_tree().current_scene.add_child(obj)

func create_projectile(creator,pos,dir,projectile):
	var obj = load(projectile).instance()
	obj.creator = creator
	obj.position = pos
	obj.direction = dir
	obj.attacker = creator.attacker
	get_tree().current_scene.add_child(obj)
	
func create_plasma_shot(creator,pos,dir):
	var obj = plasma_shots.instance()
	obj.creator = creator
	obj.position = pos
	obj.direction = dir
	get_tree().current_scene.add_child(obj)

func create_fireball(creator,pos,dir):
	var obj = fireballs.instance()
	obj.creator = creator
	obj.position = pos
	obj.direction = dir
	get_tree().current_scene.add_child(obj)

func create_headfire(creator,pos,dir):
	var obj = headfire.instance()
	obj.creator = creator
	obj.attacker = creator.attacker
	obj.position = pos
	obj.direction = dir
	get_tree().current_scene.add_child(obj)
	
	
func send_message(msg):
	if is_instance_valid(HUD):
		msg = tr(msg).replace("|","\n")
		HUD.message = msg.replace("_"," ")
		HUD.message = msg
		HUD.Messages.visible = true
		HUD.message_timer = OS.get_unix_time()

func send_message2(msg):
	if is_instance_valid(HUD):
		msg = tr(msg).replace("|","\n")
		HUD.message2 = msg.replace("_"," ")
		HUD.Messages2.visible = true
		HUD.message2_timer = OS.get_unix_time()
		
func shuffleList(list):
	var shuffledList = []
	var indexList = range(list.size())
	for i in range(list.size()):
		randomize()
		var x = randi()%indexList.size()
		shuffledList.append(list[x])
		indexList.remove(x)
		list.remove(x)
	return shuffledList

func search_files(path,ext):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
 
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(ext):
			files.append(file)
	dir.list_dir_end()
	return files


func reset_all_players():
	Global.players.clear()
	#for player in get_tree().get_nodes_in_group("players"):
	for player in range(0,PlayerSettings.data.size()):
		PlayerSettings.weapons[player] = PlayerSettings.default_weapons.duplicate(true)
		PlayerSettings.ammo[player] = PlayerSettings.default_ammo.duplicate(true)
		PlayerSettings.data[player] = PlayerSettings.default_data.duplicate(true)

func reload_map():
	hidden_tiles = []
	reset_all_players()
	get_tree().reload_current_scene()
	


func load_file(res):
	var file = File.new()
	file.open(res, File.READ)
	var content = file.get_as_text()
	file.close()
	return content

func load_settings():
	var err = config.load(settings_file)
	if err == OK: # If not, something went wrong with the file loading
		# Look for the display/width pair, and default to 1024 if missing
		var music_vol = config.get_value("audio", "music_volume",0)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music"), music_vol)
		sound_fx_vol = config.get_value("audio","sound_fx_vol",0)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("sounds"), sound_fx_vol)
		check_points = config.get_value("maps","check_points","res://maps/e1m1.tscn")
		difficulty = config.get_value("maps","difficulty",1)

func save_settings(catagory,entry,value):
	config.set_value(catagory, entry, value)
	# Save the changes by overwriting the previous file
	config.save(settings_file)

func format_time(sec):
	var h = (sec/3600); 
	var m = (sec -(3600*h))/60;
	var s = (sec -(3600*h)-(m*60));
	var time = "%02d:%02d:%02d" % [h, m, s]
	return time

func random_item():
	var items = [ 
		"boots", "Medpack", "rocket_launcher", 
		"Mega_Armor", "Mega_Sphere",
		"plasma_gun", "Shell_Box", "Soul_Sphere"]
	return shuffleList(items)[0]
	
func load_player(id):
	var player = preload("res://objects/Player.tscn").instance()
	player.player_id = id
	return player
