extends Control


func _ready():
	pass

func _input(event):
	if visible:
		if Input.is_key_pressed(KEY_Y):
			get_tree().paused = false
			Global.reset_all_players()
			get_tree().change_scene("res://UI/Main_Menu.tscn")
		elif Input.is_key_pressed(KEY_N):
			get_tree().paused = false
			if Global.dialog != null:
				Global.dialog.visible = false
