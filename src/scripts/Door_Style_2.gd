extends KinematicBody2D

export (int) var id = 0
export (float) var speed = 1
export (int) var open_delay = 0
export (int) var close_delay = 3
export (float) var movement = 128
export (String) var message = null

export(String, "none", "red key","blue key","yellow key","red skull key","blue skull key","yellow skull key") var door_type

export (String) var open_snd = "door_open"
export (String) var close_snd = "door_close"

var delay = 0
var open = false

onready var open_pos = Vector2(global_position.x,global_position.y - movement)
onready var closed_pos = global_position
var active = false
onready var tween = Tween.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(tween)
	#tween.connect("tween_completed", self, "_on_tween_completed")

func _process(delta):
	if active:
		active = false
		yield(get_tree().create_timer(open_delay),"timeout")
		delay = close_delay + speed
		open()

	if delay > 0:
		delay -= delta
	elif delay < 0:
		close()
		delay = 0

func open():
	if !open:
		open = true
		Sounds.play_sound(self,open_snd)
	tween.interpolate_property(self,"global_position",global_position,open_pos,speed,Tween.EASE_IN,Tween.EASE_IN_OUT)
	tween.start()

func close():
	open = false
	Sounds.play_sound(self,close_snd)
	tween.interpolate_property(self,"global_position",global_position,closed_pos,speed,Tween.EASE_IN,Tween.EASE_IN_OUT)
	tween.start()

#Open if player or enemy is in door
func _on_Area2D_body_entered(body):
	if message != null:
			Global.send_message2(message)
			
	if keyed(body):
			return
			
	active = true


func keyed(body):
	if door_type != "none" && door_type != "":
		if !body.keys.has(door_type):
			if message == null:
				active = false
				Global.send_message2(door_type + " is needed")
			return true
	return false


func _on_reopen_body_entered(body):
	active = true
