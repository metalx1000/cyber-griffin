extends Control
export (String,FILE) var music

onready var menu = $CanvasLayer/Menu
onready var play_btn = $CanvasLayer/Menu/VBoxContainer/Play
onready var copyright = get_node("CanvasLayer/Menu/menu_box/HBoxContainer/Copyright")
func _ready():
	load_build_info()
	Global.multi_player = false
	Global.reset_all_players()
	Music.play_resource(music)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://UI/Quit_Message.tscn")

func load_build_info():
	copyright.text = "Cyber-Griffin Copyright (c) Kris Occhipinti 2021 - License GPLv3"
	copyright.text += " - build v"
	copyright.text += load_text_file("res://build.txt")

func _on_Play_button_up():
	Global.scene_id = 0
	get_tree().change_scene(Global.scenes[0])


func _on_Credits_button_up():
	get_tree().change_scene("res://UI/Freedoom.tscn")

func load_text_file(path):
	var f = File.new()
	var err = f.open(path, File.READ)
	if err != OK:
		printerr("Could not open file, error code ", err)
		return ""
	var text = f.get_as_text()
	f.close()
	return text
