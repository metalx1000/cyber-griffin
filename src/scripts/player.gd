extends KinematicBody2D

var reload = false

export var player_id = 0

var on_lift = false
var dead = false

var invincible = false
var godmode = false
var invincible_time = 0

var teleporting = false

var direction = 0

var attacker = null
var happy = false
var hit = 0
var shocked = false
var walking = false
var walking_delay = 0
var run_power = 0.0
var shooting = 0.0
var shoot_active = false
var shooting_animation = 0.0

var weapon_delay = 0.0
var heal_time = 0
var crouching = false

var input_event
var on_floor = false

onready var camera = $Camera2D

var radiation_suit = false
var radiation_suit_time = 0
var last_touch_lava = 0

var weapon_switch = true
var velocity = Vector2.ZERO

onready var weapons = PlayerSettings.weapons[player_id]

onready var player = PlayerSettings.data[player_id]
#onready var keys = PlayerSettings.keys[player_id]
onready var keys = []

var zoom_active = false

var switch = 1

onready var body = get_node("CollisionShape2D")
onready var death_body = get_node("death_body")
onready var camera_tween = Tween.new()
onready var camera_pos_tween = Tween.new()
onready var gravity = player.gravity
onready var speed = player.speed
#onready var flame = get_node("Flame")
#ammo levels
onready var ammo = PlayerSettings.ammo[player_id]


func _ready():
	#flame.creator = self
	pick_start_pos()
	set_color()
	add_child(camera_tween)
	set_camera_bounds()
	if player.health <= 0:
		player.health = 100
	Global.players.append(self)
	$Sprite.flip_h = true
	

func set_camera_bounds():
	yield(get_tree().create_timer(.2),"timeout")
	if Global.world_bounds != null:
		var world_pos = Global.world_bounds.position
		var world_end = Global.world_bounds.end
		var cell_size = Global.world_cell_size
		if $Camera2D != null:
			$Camera2D.limit_left = world_pos.x * cell_size.x
			$Camera2D.limit_right = world_end.x * cell_size.x
			$Camera2D.limit_top = world_pos.y * cell_size.y
			$Camera2D.limit_bottom = world_end.y * cell_size.y
	
	
func _physics_process(delta):
	
	velocity.x = 0
	if !dead:
		#flame_pos()
		
		shooting -= delta
		shooting_animation -= delta
		check_radiation_suit()
		check_invinibility(delta)
		run_power_check(delta)
		touch_lava(delta)
		set_animation(delta)
		shocked()
		heal_timer(delta)
		shoot()
		
		walk(delta)
		auto_switch_weapon()

	
	if player.health < 1 && !dead:
		death()
	
	velocity.y += gravity * Global.gravity * delta
	if !teleporting:
		velocity = move_and_slide(velocity, Vector2.UP)


func check_invinibility(delta):
	if invincible_time > 0:
		invincible_time -= delta
		invincible = true
		if invincible_time > 6:
			Global.HUD.get_node("Color_Invert").visible = true
		elif invincible_time < 6 && invincible_time > 0:
			if int(invincible_time) % 2 == 0:
				Global.HUD.get_node("Color_Invert").visible = true
			else:
				Global.HUD.get_node("Color_Invert").visible = false
	else:
		if is_instance_valid(Global.HUD):
			Global.HUD.get_node("Color_Invert").visible = false
		invincible = false

func run_power_check(delta):
	if run_power > 300:
		run_power = 300
		
	if run_power > 1:
		run_power -= delta * 5
	elif run_power < 1 && run_power > 0:
		run_power = 0
		Global.send_message2("That power fuel was great!|Need to find more of that stuff!")
	

func check_radiation_suit():
	if radiation_suit:
		var current_time = OS.get_unix_time()
		if OS.get_unix_time() > radiation_suit_time:
			radiation_suit = false
			Global.HUD.color_box.visible = false
			return 1
		elif radiation_suit_time - current_time == 6:
			Global.HUD.color_box.visible = false
		elif radiation_suit_time - current_time == 5:
			Global.HUD.color_box.visible = true
		elif radiation_suit_time - current_time == 4:
			Global.HUD.color_box.visible = false
		elif radiation_suit_time - current_time == 3:
			Global.HUD.color_box.visible = true
		elif radiation_suit_time - current_time == 2:
			Global.HUD.color_box.visible = false
		elif radiation_suit_time - current_time == 1:
			Global.HUD.color_box.visible = true
		else:
			return 0
			
func touch_lava(delta):
	if !radiation_suit:
		if last_touch_lava > .5:
			last_touch_lava = 0
			for i in get_slide_count():
				var collision = get_slide_collision(i)
				if collision.collider != null:
					if collision.collider.is_in_group("lava"):
						take_damage("lava",5)
		else:
			last_touch_lava += delta
			
func _input(event):
	if !dead:
		if event.get_device() != player_id:
			return

			
		if event.is_action_pressed("jump") && !crouching:
			jump()
			
		if event.is_action_pressed("player_crouch"):
			walking = false
			crouch(true)
		
		if event.is_action_released("player_crouch"):
			crouch(false)

		if event.is_action("shoot"):
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			shoot_active = true
		
		if event.is_action_released("shoot"):
			shoot_active = false
			
		
		if event.is_action_pressed("switch_weapon_up"):
			Input.action_release("switch_weapon_up")
			Global.console = "Weapon Up"
			switch_weapon("up")
		elif event.is_action_pressed("switch_weapon_down"):
			Global.console = "Weapon Down"
			Input.action_release("switch_weapon_down")
			switch_weapon("down")
			
		if event.is_action_pressed("map"):
			camera_zoom(2,1,true)
		
		select_weapon_by_num()
		
		movements_walk(event)

	else:
		if event.is_action_pressed("shoot") && reload:
			reload = false
			if !Global.multi_player:
				Global.reload_map()
			
			
func movements_walk(event):
	if event.get_device() != player_id:
		return
	
	direction = Input.get_action_strength("walk_right") - Input.get_action_strength("walk_left")
	
func select_weapon_by_num():
	if Input.is_action_just_pressed("weapon_1"):
		jump_to_weapon(0)
	elif Input.is_action_just_pressed("weapon_2"):
		jump_to_weapon(1)
	elif Input.is_action_just_pressed("weapon_3"):
		jump_to_weapon(2)
	elif Input.is_action_just_pressed("weapon_4"):
		jump_to_weapon(3)
	elif Input.is_action_just_pressed("weapon_5"):
		jump_to_weapon(4)
	elif Input.is_action_just_pressed("weapon_6"):
		jump_to_weapon(5)
	elif Input.is_action_just_pressed("weapon_7"):
		jump_to_weapon(6)
	elif Input.is_action_just_pressed("weapon_8"):
		jump_to_weapon(7)
	elif Input.is_action_just_pressed("weapon_9"):
		jump_to_weapon(8)

func jump():		
	if $on_floor.is_colliding() or is_on_floor():
		velocity.y = player.jump_speed

func jump_to_weapon(num):
	if num < weapons.size():
		var name = weapons[num].weapon
		var i = 0
		for weapon in weapons:
			if weapon.weapon == name:
				player.weapon_num = i
				break
			i+=1
	
func camera_zoom(zoom,time,stats):
	if !zoom_active:
		zoom_active = true
		camera_tween.interpolate_property(camera, "zoom", camera.zoom, Vector2(zoom, zoom), time, Tween.EASE_IN_OUT, Tween.EASE_IN_OUT)
	else:
		zoom_active = false
		camera_tween.interpolate_property(camera, "zoom", camera.zoom, Vector2(.5, .5), time, Tween.EASE_IN_OUT, Tween.EASE_IN_OUT)
	
	if stats && !Global.multi_player:
		Global.HUD.stats_label.visible = zoom_active
	camera_tween.start()


func map_old():
	zoom_active = true
	if $Camera2D.zoom.x == 0.5:
		$Camera2D.zoom = Vector2(2,2)
	else:
			$Camera2D.zoom = Vector2(0.5,0.5)

func check_current_weapon():
	var ammo = get_ammo(weapons[player.weapon_num].ammo)
	if ammo.amount > 0:
		if weapons[player.weapon_num].active:
			return 0
	return 1
		
#checks if any avaiable weapons have ammo
func check_weapon_available():
	for w in weapons:
		var ammo = get_ammo(w.ammo)
		if w.active == true && ammo.amount > 0:
			return 0
			
	return 1

func get_ammo(type):
	for a in ammo:
		if a.type == type:
			return a
		

func auto_switch_weapon():
	if check_weapon_available():
		return 1
		
	if check_current_weapon():
		player.weapon_num += switch
		if player.weapon_num > weapons.size()-1:
			player.weapon_num = 0
		elif player.weapon_num < 0:
			player.weapon_num = weapons.size()-1
			
func switch_weapon(action):
	if !check_weapon_available():
		if weapon_switch:
			if action == "up":
				switch = 1
				weapon_switch = false
				player.weapon_num+=1
				if player.weapon_num > weapons.size()-1:
					player.weapon_num = 0
				yield(get_tree().create_timer(.1),"timeout")
				weapon_switch = true
			elif action == "down":
				switch = -1
				weapon_switch = false
				player.weapon_num-=1
				if player.weapon_num < 0:
					player.weapon_num = weapons.size()-1
				yield(get_tree().create_timer(.1),"timeout")
				weapon_switch = true
		
	


		
func crouch(state):
	if state:
		crouching = true
		$CollisionShape2D.scale.y = .5
		$CollisionShape2D.position.y = 10
		$projectile.position.y = 0
		$projectile2.position.y = 0
	else:
		crouching = false
		$CollisionShape2D.scale.y = 1
		$CollisionShape2D.position.y = 0
		$projectile.position.y = -10
		$projectile2.position.y = -10
	

func set_animation(delta):
	if hit > 0:
		hit -= delta
		$Sprite.play("hit")
	elif crouching && shooting_animation >= 0:
		$Sprite.frame = 0
		$Sprite.play("crouch_shoot")
	elif crouching:
		$Sprite.play("crouch")
	elif shooting_animation >= 0:
		$Sprite.frame = 0
		$Sprite.play("shoot")
	elif walking:
		if walking_delay <= 0:
			$Sprite.play("walk")
		else:
			walking_delay -= 1
	else:
		$Sprite.play("default")

func take_damage(creator,damage):
	if Global.difficulty > 4:
		damage = damage * 2
		
	if Global.multi_player:
		damage = damage/2
		
	if !invincible && !dead && !godmode:
		Sounds.play_sound(self,"player_hit")
		attacker = creator
		
		if player.armor > 0:
			damage = damage / 2
			player.armor -= damage
			if player.armor < 0:
				player.armor = 0
		if damage < 1:
			damage = 1
		
		player.health -= damage
		hit = .25
		shocked = true
		
func shocked():
	if shocked:
		#display's a shocked face
		yield(get_tree().create_timer(.25),"timeout")
		shocked = false


func walk(delta):
	if !crouching:
		velocity.x = direction * speed * delta + (run_power*direction)
		walking = true
		if direction > 0.5:
			$Sprite.flip_h = true
		elif direction < -0.5:
			$Sprite.flip_h = false
		else:
			walking = false
			
func puff():
	var space_state = get_world_2d().direct_space_state
	# use global coordinates, not local to node
	var tar_distance = 1000
	if $Sprite.flip_h:
		tar_distance = 1000
	else:
		tar_distance = -1000
		
	var target = Vector2(global_position.x + tar_distance, global_position.y + rand_range(-100,100))
	var pos = Vector2(global_position.x,global_position.y - 10)
	var result = space_state.intersect_ray(pos, target, [self])
	if result:
		Global.create_puff(result.position,self)
	#shooting complete

func total_ammo():
	var total = 0
	for w in weapons:
		var ammo = get_ammo(w.ammo)
		total += ammo.amount
	return total
	
func shoot():
	if shooting <= 0 && shoot_active:
		walking_delay = 10
		if total_ammo() > 0:
			if weapons[player.weapon_num].weapon == "shotgun" && weapons[player.weapon_num].active:
				shoot_shotgun()
				shooting = .75
			if weapons[player.weapon_num].weapon == "double_barrel" && weapons[player.weapon_num].active:
				shoot_double_barrel()
				shooting = 1.5
			elif weapons[player.weapon_num].weapon == "chaingun" && weapons[player.weapon_num].active:
				shoot_chaingun()
				shooting = .1
			elif weapons[player.weapon_num].weapon == "pistol" && weapons[player.weapon_num].active:
				shoot_pistol()
				shooting = .5
			elif weapons[player.weapon_num].weapon == "plasma_gun" && weapons[player.weapon_num]:
				shoot_plasma()
				shooting = .1
			elif weapons[player.weapon_num].weapon == "rocket_launcher" && weapons[player.weapon_num]:
				shoot_rocket()
				shooting = .5
		

func heal_timer(delta):
	#healing factor - like wolverine
	if player.heal:
		heal_time += delta
		if heal_time > 5:
			heal_time = 0
			player.health += 1
	
func shoot_plasma():
	var ammo = get_ammo("plasma")
	if ammo.amount > 0:
		shooting_animation = .2
		ammo.amount -= 1
		var direction = get_direction()
		var pos = $projectile.global_position
		if direction < -0.5:
			pos = $projectile2.global_position
			
		Global.create_plasma_shot(self,pos,direction)
		if ammo.amount <= 0:
			ammo.amount = 0
			switch_weapon("down")
	else:
		Global.send_message("Out of Ammo")
				
func shoot_rocket():
	var ammo = get_ammo("rockets")
	if ammo.amount > 0:
		shooting_animation = .2
		ammo.amount -= 1
		var direction = get_direction()
		var pos = $projectile.global_position
		if direction < -0.5:
			pos = $projectile2.global_position
			
		Global.create_projectile(self,pos,direction,"res://objects/rock_fire.tscn")
		if ammo.amount <= 0:
			ammo.amount = 0
			switch_weapon("down")
	else:
		Global.send_message("Out of Ammo")

func shoot_shotgun():
	var ammo = get_ammo("shells")
	if ammo.amount > 0:
		shooting_animation = .2
		ammo.amount -= 1
		Sounds.play_sound(self,"shotgun")
		for i in range(0,4):
			puff()
		if ammo.amount <= 0:
			ammo.amount = 0
			switch_weapon("up")
	else:
		Global.send_message("Out of Ammo")

func shoot_double_barrel():
	var ammo = get_ammo("shells")
	if ammo.amount > 1:
		shooting_animation = .2
		ammo.amount -= 2
		Sounds.play_sound(self,"double_barrel")
		for i in range(0,12):
			puff()
		if ammo.amount <= 0:
			ammo.amount = 0
			switch_weapon("up")
	else:
		switch_weapon("up")
		Global.send_message("Out of Ammo")
		
func shoot_pistol():
	var a = get_ammo("bullets_p")
	if a.amount > 0:
		shooting_animation = .2
		a.amount -= 1
		#shooting = true
		Sounds.play_sound(self,"pistol")
		puff()
		if a.amount <= 0:
			a.amount = 0
	else:
		Global.send_message("Out of Ammo")

func get_direction():
	if $Sprite.flip_h:
		return 1
	else:
		return -1

func shoot_chaingun():
	var ammo = get_ammo("bullets_c")
	if ammo.amount > 0:
		shooting_animation = .2
		ammo.amount -= 1
		#shooting = true
		Sounds.play_sound(self,"pistol")
		puff()
		if ammo.amount <= 0:
			ammo.amount = 0
	else:
		Global.send_message("Out of Ammo")

func make_happy():
	#:)
	happy = true
	yield(get_tree().create_timer(1),"timeout")
	happy = false

func reset_player():
	PlayerSettings.weapons[player_id] = PlayerSettings.default_weapons
	PlayerSettings.ammo[player_id] = PlayerSettings.default_ammo
	player.armor = 0

func pick_start_pos():
	#yield(get_tree().create_timer(.5),"timeout")
	if Global.multi_player:
		var start_group = get_tree().get_nodes_in_group("player_start")
		start_group = Global.shuffleList(start_group)
		
		for pos in start_group:
			if pos.id == player_id:
				pos.active = true
				global_position = pos.global_position
				return true
				
		var pos = start_group[0]
		pos.active = true
		global_position = pos.global_position
		return true
		
func set_color():
	if player_id == 0:
		pass
	elif player_id == 1:
		hue_shift(.7)
	elif player_id == 2:
		hue_shift(.3)
	elif player_id == 3:
		hue_shift(.4)
	else:
		var rand_hue = float(randi() % 3)/2.0/3.2
		hue_shift(rand_hue)
		
func hue_shift(hue):
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	$Sprite.set_material($Sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	$Sprite.material.set_shader_param("Shift_Hue", hue)

func respawn(delay):
	weapons = PlayerSettings.default_weapons.duplicate(true)
	yield(get_tree().create_timer(delay/2),"timeout")
	if pick_start_pos():
		yield(get_tree().create_timer(delay/2),"timeout")
		dead = false
		player.health = 100
		death_body.disabled = true
		body.disabled = false
		set_collision_layer_bit(1,true)

func drop_item(item):
	#1 in 10 chances the item is a random special item
	if rand_range(0,10) > 9:
		item = "random"

	#check if item exist
	var file2Check = File.new()
	if !file2Check.file_exists(item):
		item = Global.random_item()
	
	item = "res://objects/" + item + ".tscn"

	item = load(item).instance()
	get_tree().current_scene.add_child(item)
	item.position = global_position
	
func death():
	if !dead:
		reset_player()
				#remvoe from player layer 
		set_collision_layer_bit(1,false)

		death_body.disabled = false
		body.disabled = true
		shoot_active = false
		shooting = 0.0
		dead = true
		Sounds.play_sound(self,"player_death")
		player.health = 0
		velocity.x = 0
		$Sprite.play("death")
		if Global.multi_player:
			drop_item(weapons[player.weapon_num].weapon)
			respawn(3)
		yield(get_tree().create_timer(1),"timeout")
		
		reload = true
