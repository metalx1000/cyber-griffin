extends AnimatedSprite

export var id = 1
export (bool) var sound = true
func _ready():
	visible = false
	pass


func activate():
	if sound:
		Sounds.play_sound(self,"teleporter")
	visible = true
	frame = 0
	play("default")
	pass # Replace with function body.
