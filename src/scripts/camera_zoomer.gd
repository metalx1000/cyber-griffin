extends Area2D
export (float) var zoom_level = 1
export (float) var zoom_time = 2
export (float) var offset_h = 0
export (float) var offset_v = 0
export (float) var smoothing_speed = 5

func _ready():
	pass


func _on_camera_zoomer_body_entered(body):
	var camera = body.get_node("Camera2D")
	body.zoom_active = false
	body.camera_zoom(zoom_level,zoom_time,false)
	camera.smoothing_speed = smoothing_speed
	#yield(get_tree().create_timer(zoom_time),"timeout")
	camera.offset_v = offset_v
	camera.offset_h = offset_h
	
	
	pass # Replace with function body.
