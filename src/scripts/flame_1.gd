extends Particles2D
onready var ray = get_node("raycast")
onready var ani = get_node("ani")

var creator
export (int) var direction = 1


var damage = 1

func _ready():
	ani.play("left")
	ani.get_animation("left").set_loop(true)
	ani.get_animation("right").set_loop(true)
	
	
func _process(delta):
	ray_direction()
	hit_detect()
	
	
func hit_detect():
	var dis = 1000 * direction
	if ray.is_colliding():
		var pos = ray.get_collision_point()
		var body = ray.get_collider()
		if body != creator:
			dis = global_position.distance_to(pos) * 2 * direction
		hit(body)
	
	process_material.set("initial_velocity",dis)

func ray_direction():
	if direction == 1:
		ani.play("left")
	else:
		ani.play("right")
		
func hit(body):
	if body.has_method("take_damge"):
		body.take_damge(self,damage)
