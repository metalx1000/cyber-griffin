extends Sprite
export (float) var speed = -40

var objects = []
func _ready():
	pass

func _process(delta):
	for obj in objects:
		obj.velocity.x = speed
		obj.velocity = obj.move_and_slide(obj.velocity, Vector2.UP)
		if obj.get("time_out") != null:
			obj.time_out = 0

func _on_Area2D_body_entered(body):
	objects.append(body)


func _on_Area2D_body_exited(body):
	objects.erase(body)
	pass # Replace with function body.
