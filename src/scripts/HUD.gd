extends Control

onready var Ammo = get_node("bottom_hud/ammo")
onready var Ammo2 = get_node("bottom_hud/ammo2")
onready var current_weapon = get_node("bottom_hud/current_weapon")
onready var Console = get_node("console")

onready var Messages = get_node("messages")
var message_timer = OS.get_unix_time()
var message

onready var Messages2 = get_node("messages2")
var message2_timer = OS.get_unix_time()
var message2

onready var stats_label = get_node("stats")

onready var keybox = get_node("bottom_hud/keybox")
onready var player = get_tree().get_nodes_in_group("players")[0]

onready var armor = get_node("bottom_hud/armor")
onready var armor_label = get_node("bottom_hud/armor_label")
onready var health_label = get_node("bottom_hud/health_label")

var color_box

# Called when the node enters the scene tree for the first time.
func _ready():
	create_color_screen()
	
	hide_all_keys()
	Messages.visible = false
	Messages2.visible = false
	visible = true
	Global.HUD = self

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Global.players.size() > 0:
		player = get_tree().get_nodes_in_group("players")[0]
	if player == null:
		return
	armor_check()
	check_keys()
	message_timeout()
	health_label.text = str(player.player.health)
	
	#Info.text = "Health: " + str(PlayerSettings.data[0].health) + "\n"
	stats_hud(delta)
	
	ammo_hud()

	Console.text = str(Global.console)

	Messages.text = str(message)
	Messages2.text = str(message2)

func stats_hud(delta):
	#level time
	Global.level_time += delta
	var time = int(Global.level_time)
	time = Global.format_time(time)
	stats_label.text = "Level Time: "+str(time)
	#enemy stats
	Global.total_enemies = get_tree().get_nodes_in_group("enemies").size()
	stats_label.text += "\nEnemies: "+ str(Global.enemies_killed)+"/"+str(Global.total_enemies)
	
	#secert stats
	stats_label.text += "\nSecrets Found: " + str(Global.secrets_found) + "/"+ str(Global.total_secrets)


func ammo_hud():
	Ammo.text = ""
	Ammo2.text = ""
	#get name of current weapon
	var name = str(PlayerSettings.weapons[0][PlayerSettings.data[0].weapon_num].weapon.replace("_"," "))
	
	var i = 0
	for weapon in PlayerSettings.weapons[0]:
		i += 1
		if weapon.active:
			var ammo = player.get_ammo(weapon.ammo)
			var w = str(weapon.weapon.replace("_"," "))
			if w == name:
				current_weapon.text = name+":\n" + str(ammo.amount)
			if ammo.amount > 0:
				if i % 2 == 0:
					Ammo2.text += w + ": " + str(ammo.amount) + "\n"
				else:
					Ammo.text += w + ": " + str(ammo.amount) + "\n"
func check_keys():
	for key in player.keys:
		var k = keybox.get_node(key)
		k.visible = true

func hide_all_keys():
	for key in keybox.get_children():
		key.visible = false

func message_timeout():
	if OS.get_unix_time() - message_timer > 3:
		Messages.visible = false
	if OS.get_unix_time() - message2_timer > 2:
		Messages2.visible = false
		
func armor_check():
	armor_label.text = str(player.player.armor)
	armor_label.visible = armor.visible
	if player.player.armor > 0:
		if player.player.armor <= 100:
			armor.play("armor")
			armor.visible = true
		elif player.player.armor > 100:
			armor.play("mega_armor")
			armor.visible = true
	else:
		armor.visible = false
	
	

func create_color_screen():
	var box = ColorRect.new()
	box.color = Color(0, 1, 0, 0.2)
	box.margin_right = get_viewport().size.x
	box.margin_left = -get_viewport().size.x/2
	box.margin_bottom = get_viewport().size.y
	box.margin_top = - get_viewport().size.y/2
	box.visible = false
	add_child(box)
	color_box = box
	return box
	

