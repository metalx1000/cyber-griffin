extends Control

func _ready():
	visible = true
	pass
	
func _process(delta):
	for player in get_tree().get_nodes_in_group("players"):
		var id = player.player_id
		var label = get_tree().get_nodes_in_group("hud_2_label")[id]
		var health = "Health: " + str(player.player.health) + "\n"
		var weapon = str(PlayerSettings.weapons[id][PlayerSettings.data[id].weapon_num].weapon.replace("_"," "))
		var ammo = weapon + ": " + str(player.get_ammo(player.weapons[player.player.weapon_num].ammo).amount)
		var armor = "\nArmor: " + str(player.player.armor) + "\n"
		label.text = health + ammo + armor
