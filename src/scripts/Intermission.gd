extends Control
export (String,FILE) var next_scene
export (String,FILE) var music

var background
var text
var clicks = 0

func _ready():
	load_music()
	$AnimationPlayer.play("Text Type")
	$Continue.reference_path = next_scene

func load_music():
	#check if file exists, if not play random track
	if ResourceLoader.exists(music):
		Music.play_resource(music)
	else:
		Music.random_track()

func next():
	yield(get_tree().create_timer(.2),"timeout")
	if next_scene == "":
		get_tree().change_scene("res://UI/Main_Menu.tscn")
	else:
		get_tree().change_scene(next_scene)

func _input(event):
	if Input.is_action_just_pressed("jump") || Input.is_action_just_pressed("shoot"):
		Sounds.play_sound(null,"shotgun")
		clicks += 1
		if clicks == 1:
			var length = $AnimationPlayer.get_current_animation_length()
			$AnimationPlayer.seek(length)
		elif clicks == 2:
			next()
