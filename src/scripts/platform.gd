extends KinematicBody2D

export (bool) var active = false
export (float) var speed = 1

func _ready():
	$AnimationPlayer.playback_speed = speed
	$AnimationPlayer.seek(0)
	pass
	
func _physics_process(delta):
	if active:
		$AnimationPlayer.play("moveplatform")

func _on_top_body_entered(body):
	active = true
