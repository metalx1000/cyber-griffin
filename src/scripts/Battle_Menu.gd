extends Control
onready var container = get_node("CanvasLayer/Menu/menu_box/VBoxContainer")
var buttons = preload("res://UI/Button.tscn")
func _ready():
	var maps = search_files("res://maps/battles/",".tscn")
	maps.sort_custom(self, "customComparison")
	for map in maps:
		var scene = "res://maps/battles/" + map
		var button = buttons.instance()
		button.text = map.get_basename().replace("_"," ")
		button.reference_path = scene
		container.add_child(button)
	pass

#sort array
func customComparison(a, b):
	if typeof(a) != typeof(b):
		return typeof(a) < typeof(b)
	else:
		return a < b
		
func search_files(path,ext):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
 
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(ext):
			files.append(file)
	dir.list_dir_end()
	return files
