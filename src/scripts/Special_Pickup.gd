extends Area2D
export(bool) var secret_area = false
export var msg = ""
export var msg2 = ""
export (String) var sound = "bonus"
export (bool) var happy = false
export (bool) var shocked = false
export (bool) var die = true

func _ready():
	if secret_area:
		die = true
		yield(get_tree().create_timer(.5),"timeout")
		Global.total_secrets += 1



func _on_Special_Pickup_body_entered(body):
	if happy:
		body.make_happy()
	elif shocked:
		#body.shocked = true
		pass
		
	if secret_area:
		body.player.secrets += 1
		Global.secrets_found += 1
		msg = "Secrets Found: " + str(Global.secrets_found) + "/"+ str(Global.total_secrets)
	
	if sound != "":
		var snd = Sounds.play_sound(self,sound)
		
	Global.send_message2(msg2)
	Global.send_message(msg)
	if die:
		#yield(get_tree().create_timer(2),"timeout")
		queue_free()
