extends Area2D
export var id = 0
export (String) var msg
export (bool) var set = true


func _ready():
	pass


func _on_hidden_revealer_body_entered(body):
	if set:
		set = false
		Global.send_message2(msg)
		for hidden in get_tree().get_nodes_in_group("hidden_tiles"):
			if hidden.id == id:
				var tween = Tween.new()
				add_child(tween)
				tween.interpolate_property(hidden, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 1 , Tween.EASE_IN_OUT, Tween.EASE_IN_OUT)
				tween.start()
