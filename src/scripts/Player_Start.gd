extends Node2D
export var id = 0
var active = false
onready var sprite = get_node("Sprite")

func _ready():
	z_index = 10
	sprite.visible = false
	add_to_group("player_start")
	pass

func _process(delta):
	if active:
		active = false
		sprite.frame = 0
		sprite.play("default")
		sprite.visible = true
		Sounds.play_sound(self,"teleporter")

	if sprite.frame == sprite.frames.get_frame_count("default") - 1:
		sprite.visible = false
