extends KinematicBody2D

var gravity = 1

var velocity = Vector2.ZERO
export (String, "none","red key","yellow key","blue key","red skull key","yellow skull key","blue skull key" ) var key_type
#onready var HUDkey = Global.HUD.keybox


func _ready():
	
	pass
	
		
func _physics_process(delta):

	velocity.y += gravity * Global.gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	
func pickup(body):
	if body != self && body.is_in_group("players"):
		if !body.dead:
			#every player with same id gets the key
			for player in get_tree().get_nodes_in_group("players"):
				if player.player_id == body.player_id:
					Global.send_message(key_type + " Pickup")
					Sounds.play_sound(body,"bonus")
					player.keys.append(key_type)
					queue_free()
					
