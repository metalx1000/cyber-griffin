extends Node

var stream_player = AudioStreamPlayer.new()
var current_track = 0
var music_playing = true
onready var current_scene = get_tree().get_current_scene().get_name()

var tracks = Global.search_files("res/music",".ogg")

func _ready():
	stream_player.set_bus("music")
	pass

func _process(delta):
	if Input.is_action_just_pressed("music_next"):
		change_track(1)
	elif Input.is_action_just_pressed("music_prev"):
		change_track(-1)
	elif Input.is_action_just_pressed("music_toggle"):
		music_toggle()

func random_track():
	current_track = int(rand_range(0,tracks.size()))
	play_track(current_track)

func check_track(num):
	if num > tracks.size()-1:
		print("bad track number")
		return false
		
	if num < 0:
		print("bad track number")
		return false
	
	return true
	
func change_track(num):
	num = current_track + num
	var check = check_track(num)
	if check:
		play_track(num)
		current_track = num
	else:
		print("Track #" +str(num)+ " does not exist...")

func music_toggle():
	if stream_player.playing:
		music_playing = false
		stream_player.stop()
	else:
		music_playing = true
		stream_player.play()

func play_resource(file):
	var track = file
	if ResourceLoader.exists(track):
		
		stream_player.stream = ResourceLoader.load(track)
		stream_player.stream.set_loop(true)
		
		#stream_player.connect("finished", stream_player, "queue_free")
		add_child(stream_player)
		stream_player.play()
	else:
		print(track, " does not exist.")

func play_track(num):
	var track = "res://res/music/" + tracks[num]
	if ResourceLoader.exists(track):
		
		stream_player.stream = ResourceLoader.load(track)
		stream_player.stream.set_loop(true)
		
		#stream_player.connect("finished", stream_player, "queue_free")
		add_child(stream_player)
		stream_player.play()
	else:
		print(track, " does not exist.")
		
func fade_out():
	var v = stream_player.volume_db
	while v > -80:
		stream_player.volume_db -= .2
		yield(get_tree().create_timer(.01),"timeout")
