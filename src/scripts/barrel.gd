extends KinematicBody2D

export (int) var gravity = 1000
export (int) var life = 5

var dead = false
var attacker = null
var velocity = Vector2.ZERO
var speed = 0
var onscreen = false

func _physics_process(delta):
	if onscreen:
		velocity.y += gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
	if life < 1 && !dead:
		death()

func death():
	dead = true

	var death_delay = rand_range(0.00,.30)
	$Body.disabled = true
	yield(get_tree().create_timer(death_delay),"timeout")
	$Area2D/CollisionShape2D.disabled = false
	$Area2D2/CollisionShape2D.disabled = false
	$Area2D3/CollisionShape2D.disabled = false
	$body_explode/AnimationPlayer.play("explode")
	$Sprite.play("death")

	Sounds.play_sound(self,"explode")
	yield(get_tree().create_timer(.2),"timeout")
	$Area2D/CollisionShape2D.disabled = true
	$Area2D2/CollisionShape2D.disabled = true
	$Area2D3/CollisionShape2D.disabled = true
	yield(get_tree().create_timer(.3),"timeout")
	queue_free()

func take_damage(creator,damage):
	life -= damage
	attacker = creator

func hit(body):
	if body != self && body.is_in_group("shootable"):
		body.take_damage(self,10)


func _on_VisibilityNotifier2D_screen_entered():
	onscreen = true


func _on_VisibilityNotifier2D_screen_exited():
	onscreen = false
