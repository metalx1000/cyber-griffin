extends Control

var msgs = [
	"So, You're scared.  That's understandable.  Go ahead and run away.",
	"The Cyber-Griffin can smell your fear",
	"HA-HA-HA! Run away",
	"Are you crying?  I guess the Cyber-Griffin is to much for you.",
	"What's that smell?  Oh, it's your fear.",
]

func _ready():
	randomize()
	var msg = msgs[randi() % msgs.size()]
	$MSG.text = msg
	$animate.play("fade")
	Music.fade_out()
	Global.play_sound(null,"res://res/sounds/cyber_griffin.ogg")
	print(Global.load_file("res://griffin.txt"))
	yield(get_tree().create_timer(5),"timeout")
	get_tree().quit()
