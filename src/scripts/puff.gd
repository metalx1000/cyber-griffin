extends Node2D

var creator = null
var damage = 2
func _ready():
	$Sprite.play("default")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func death():
	queue_free()
	pass # Replace with function body.


func hit(body):
	if body != self && body != creator:
		if body.is_in_group("shootable"):
			if body.is_in_group("bleed"):
				$Sprite.play("blood")
				body.attacker = creator
			if body.has_method("take_damage"):
				
				body.take_damage(creator,damage)
	
