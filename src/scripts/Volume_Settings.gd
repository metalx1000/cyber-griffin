extends Control


func _ready():
	var music_vol = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("music"))
	$CanvasLayer/Menu/VBoxContainer/Music_Control/Music_Slider.value = music_vol
	$CanvasLayer/Menu/VBoxContainer/Sound_Control/Sound_Slider.value = Global.sound_fx_vol
	$CanvasLayer/Menu/VBoxContainer/Sound_Control/Sound_Label.focus_mode
	
func _on_Music_Slider_value_changed(value):
	
	Global.save_settings("audio","music_volume",value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music"), value)
	#Global.get_volume_percent(value,"music")
	Sounds.play_sound(null,"menu")

func _on_Sound_Slider_value_changed(value):
	Global.sound_fx_vol = value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("sounds"), value)
	Global.save_settings("audio","sound_fx_vol",value)
	#Global.get_volume_percent(value,"FX")
	Sounds.play_sound(null,"menu")


func slider_focus_entered():
	Sounds.play_sound(null,"menu")

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		Global.play_sound(null,"res://res/sounds/menu_select.ogg")
		get_tree().change_scene("res://UI/Main_Menu.tscn")
