extends KinematicBody2D

var gravity = 1
var onscreen = false
var time_out = 0

var velocity = Vector2.ZERO
export (int) var value = 100
export (int) var max_value = 10000
export (String) var type = "Power Fuel For Suit"


func _physics_process(delta):
	time_out += delta
	if time_out < 2 || onscreen:
		velocity.y += gravity * Global.gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
	
func pickup(body):
	if body != self && body.is_in_group("players"):
		if body.run_power < max_value:
			Global.send_message(type + " Pickup")
			Sounds.play_sound(body,"bonus")
			queue_free()
			body.run_power += value
			if body.run_power > max_value:
				body.run_power = max_value

func _on_onscreen_screen_entered():
	onscreen = true


func _on_onscreen_screen_exited():
	onscreen = false
	time_out = 0
