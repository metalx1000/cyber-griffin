extends KinematicBody2D

export (int) var speed = 800
export (int) var speed_up = 0

var diff = Global.difficulty

var attacker = null
var target = null

export (bool) var tracker = false

export (int) var damage = 15
export (String) var alert_snd
export (String) var death_snd
export (float) var free_time = .5
export (float) var follow_delay = .5
var delay = 0


var creator = null
var dead = false
var direction = Global.difficulty

func _ready():
	Sounds.play_sound(self,"laser")
	$Sprite.play("default")
	set_direction()
	yield(get_tree().create_timer(.1),"timeout")
	if alert_snd != "":
		Sounds.play_sound(self,alert_snd)
	
	pass

func _process(delta):
	if is_instance_valid(creator):
		if !creator.is_in_group("players"):
			diff = float(Global.difficulty)/2
		else:
			#keep projectile speed Consistent for player
			diff = 2
			
	if is_instance_valid(attacker) && tracker:
		follow_target(delta)
	else:
		position.y += speed_up * diff * delta 
	
	position.x += speed * ( diff * .8 ) * delta * direction 
		

func follow_target(delta):
	delay -= delta
	if target == null:
		target = attacker.global_position
	if delay <= 0:
		delay = follow_delay
	
	position.y = move_toward(global_position.y,target.y,1)
	#position = lerp(global_position,target,.01)
		
		
func set_direction():
	if direction == -1:
		$Sprite.flip_h = true
	else:
		$Sprite.flip_h = false

func _death():
	pass

func damage(body):
	if dead:
		if body.is_in_group("shootable"):
			body.take_damage(self,damage)

func death():
	if !dead:
		if death_snd != "":
			Sounds.play_sound(self,death_snd)
		_death()
		dead = true
		speed = 0
		speed_up = 0
		$Sprite.play("death")
		yield(get_tree().create_timer(free_time),"timeout")
		queue_free()
	

func _on_Area2D_body_entered(body):
	if body != creator && !dead:

		if $AnimationPlayer:
			$AnimationPlayer.play("death")
		if body.is_in_group("shootable"):
			if body.has_method("take_damage"):
				body.take_damage(self,damage)
			else:
				body.get_owner().activate()
		death()


func _on_Area2D2_area_entered(area):

	pass # Replace with function body.
