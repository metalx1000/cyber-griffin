extends Node2D

export (int) var id = 0
export (int) var useage_times = -1
export (bool) var shootable = false
export (float) var delay = 0

func _ready():
	if shootable:
		$Area2D.set_collision_layer_bit(7,true)
		$Area2D.set_collision_mask_bit(4,true)
		$Area2D.set_collision_mask_bit(1,false)
	else:
		$Area2D.set_collision_mask_bit(1,true)
		$Area2D.set_collision_mask_bit(4,false)
		
	if $sprite != null:
		$sprite.play("off")
	pass

	
func _on_Area2D_body_entered(body):
	activate()
	
func activate():
	if useage_times != 0:
		useage_times -= 1
		yield(get_tree().create_timer(delay),"timeout")
		for creator in get_tree().get_nodes_in_group("object_creators"):
			if creator.id == id:
				creator.active = true
				if $sprite != null:
					$sprite.play("on")
					Sounds.play_sound(self,"menu_select")
