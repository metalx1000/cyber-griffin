extends Node

var cheatcode = ""
var cheat_count = 0

onready var players = get_tree().get_nodes_in_group("players")

const CODE_KEYS := {
	KEY_SPACE:" ", KEY_APOSTROPHE:"'", KEY_COMMA:",", KEY_MINUS:"-", KEY_PERIOD:".", KEY_SLASH:"/",
	KEY_0:"0", KEY_1:"1", KEY_2:"2", KEY_3:"3", KEY_4:"4", KEY_5:"5", KEY_6:"6", KEY_7:"7", KEY_8:"8", KEY_9:"9",
	KEY_SEMICOLON:";", KEY_EQUAL:"=",

	KEY_A:"a", KEY_B:"b", KEY_C:"c", KEY_D:"d", KEY_E:"e", KEY_F:"f", KEY_G:"g", KEY_H:"h", KEY_I:"i",
	KEY_J:"j", KEY_K:"k", KEY_L:"l", KEY_M:"m", KEY_N:"n", KEY_O:"o", KEY_P:"p", KEY_Q:"q", KEY_R:"r",
	KEY_S:"s", KEY_T:"t", KEY_U:"u", KEY_V:"v", KEY_W:"w", KEY_X:"x", KEY_Y:"y", KEY_Z:"z",

	KEY_BRACELEFT:"[", KEY_BACKSLASH:"\\", KEY_BRACERIGHT:"]", KEY_QUOTELEFT:"`"
}

func _ready():
	pass

func _input(event: InputEvent) -> void:
		
	if event is InputEventKey and event.scancode in CODE_KEYS:
		var key_string: String = CODE_KEYS[event.scancode]
		if event.pressed and not event.is_echo():
			if key_string == "i":
				cheatcode = "i"
			else:
				cheatcode += key_string
				
			check_cheatcode()

func happy_ammo(player):
	for ammo in player.ammo:
		ammo.amount = ammo.max_amount

func happy_weapon(player):
	for weapon in player.weapons:
		weapon.active = true

func cheater_check():
	cheat_count += 1
	if cheat_count == 10:
		yield(get_tree().create_timer(1),"timeout")
		Global.send_message2("You're a Cheater!!!")
	elif cheat_count == 20:
		yield(get_tree().create_timer(1),"timeout")
		Global.send_message("Mega Cheater!!!")
	elif cheat_count == 50:
		yield(get_tree().create_timer(1),"timeout")
		Global.send_message("Ultra Cheater!!!")

func unlock_levels():
	var maps = search_files("res://maps/",".tscn")
	maps.sort_custom(self, "customComparison")
	Global.check_points = []
	for map in maps:
		map = "res://maps/" + map
		Global.check_points.append(map)
		Global.save_settings("maps","check_points",Global.check_points)

func customComparison(a, b):
	if typeof(a) != typeof(b):
		return typeof(a) < typeof(b)
	else:
		return a < b
		
func search_files(path,ext):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
 
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(ext):
			files.append(file)
	dir.list_dir_end()
	return files	


func check_cheatcode():
	players = get_tree().get_nodes_in_group("players")
	if cheatcode == "iddqd":
		for player in players:
			if !player.godmode:
				Global.send_message("GOD MODE")
				player.godmode = true
				player.player.health = 100
				cheater_check()
			else:
				Global.send_message("GOD MODE OFF")
				player.godmode = false
				
	#JUMP CHEAT
	elif cheatcode == "idjump":
		for player in players:
			player.make_happy()
			if player.player.jump_speed == PlayerSettings.default_data.jump_speed:
				Global.send_message("Super Jump On")
				player.player.jump_speed = -800
				cheater_check()
			else:
				Global.send_message("Super Jump Off")
				player.player.jump_speed = PlayerSettings.default_data.jump_speed
	#Weapons and Ammo CHEAT
	elif cheatcode == "idfa":
		cheater_check()
		for player in players:
			player.make_happy()
			happy_ammo(player)
			happy_weapon(player)
			Global.send_message("Happy Weapons and Ammo")
			
	#KEY & AMMO CHEAT
	elif cheatcode == "idkfa":
		cheater_check()
		
		for player in players:
			player.make_happy()
			happy_ammo(player)
			happy_weapon(player)
			Global.send_message("All Keys and Ammo Added")
			player.keys = ["blue key","red key","yellow key","red skull key","blue skull key","yellow skull key"]
			
	#FLY mode
	elif cheatcode == "idfly":
		cheater_check()
		var fly_gravity = 0.1
		for player in players:
			if player.gravity == fly_gravity:
				Global.send_message("Low Gravity Mode Off")
				player.gravity =  PlayerSettings.default_data.gravity
			else:
				player.make_happy()
				Global.send_message("Low Gravity Mode On")
				Global.send_message2("Watch your head")
				player.gravity = fly_gravity
				
	#Healing factor
	elif cheatcode == "idheal":
		cheater_check()
		for player in players:
			if player.player.heal:
				player.player.heal = false
				Global.send_message("Wolverine Mode Off")
			else:
				player.make_happy()
				Global.send_message("Wolverine Mode")
				player.player.heal = true
	
		#power fuel 
	elif cheatcode == "idrun":
		cheater_check()
		for player in players:
			player.run_power += 200
			Global.send_message("Power Fuel Increased to " + str(player.run_power))
			
	elif cheatcode == "idclev":
		cheater_check()
		for end in get_tree().get_nodes_in_group("end_level"):
			for player in players:
				player.global_position = end.global_position
	
	elif cheatcode == "idmaps":
		cheater_check()
		unlock_levels()
