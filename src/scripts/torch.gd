extends KinematicBody2D

export (int) var gravity = 1000
#var list = $Sprite.frames.get_animation_names().size
export(String, "red","blue","green") var object 
var velocity = Vector2.ZERO

func _ready():
	$Sprite.play(object)
	pass

func _physics_process(delta):
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
