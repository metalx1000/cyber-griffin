extends KinematicBody2D

export (int) var speed = 100
export (int) var life = 10
export (float) var attack_delay = 0.0
export (bool) var site_sound = true
export (bool) var pause_on_hit = false
export (bool) var fly = false
export (bool) var free_on_death = false

export var turn_delay = 3
onready var turn_delay_set = turn_delay

var fly_time = 2.0

export (int) var melee_damage = 5

export var snd_site = ["enemy_pos_site_1","enemy_pos_site_2","enemy_pos_site_1"]
export var snd_pain = ["enemy_pos_pain_1"]
export var snd_melee = ["enemy_pink_bite"]
export var snd_death = ["enemy_pos_death_1","enemy_pos_death_3","enemy_pos_death_3"]
export var snd_act = ""

export (String,FILE) var drop_item
export (String) var weapon = "pistol"

onready var start_life = life

var onscreen = false
var offscreen = 0
onready var last_pos = position.x

var gravity = 1

export (String,"left","right") var facing



var attacker = null
var attack = true
var attacking = false
var attack_animation = false
var walk_animation = false
var hit_animation = false

onready var body = get_node("CollisionShape2D2")
onready var area = get_node("Area2D/CollisionShape2D")
onready var death_body = get_node("death_body")

var turning = false
var following = true

var playing_snd = false


var velocity = Vector2.ZERO


var time_out = 0
var hit = false
var active = false

var dead = false
var direction = 1
var last_seen = 0.0
var check_player_near_time = rand_range(1,3)
var last_snd = OS.get_unix_time()

func _ready():
	life = life * Global.difficulty
	speed = speed * Global.difficulty + rand_range(100,500)
	if facing == "right":
		direction = -1
		$Sprite.flip_h = true
	colide_with_player()

func colide_with_player():
	#on higher difficulty setting make some enemies block your path
	var level = Global.difficulty
	level = rand_range(0,level)
	if level > 2:
		set_collision_mask_bit(1,true)
	
func _physics_process(delta):
	if life < 1 && !dead:
		death()
	fly(delta)
	if active:
		if attacker.dead:
			active = false
		if turn_delay > 0:
			turn_delay -= delta
		else:
			turn_delay = turn_delay_set
			face_position(attacker.global_position.x)
		
		
		walk(delta)
		
		#turn(delta)
		play_act_snd(delta)
		#check_last_seen(delta)
	else:
		walk_animation = false

	if onscreen:
		hit_scanner()
		check_wall()
		check_player_near(delta)
		#if active:
		velocity.y += gravity * Global.gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
	else:
		time_out += delta
		if time_out < 2 || onscreen:
			velocity.y += gravity * Global.gravity * delta
			velocity = move_and_slide(velocity, Vector2.UP)
	
	animations()
#	else:
#		offscreen -= delta
#		if offscreen <= 0:
#			active = false

func fly(delta):
	if fly && !dead:
		fly_time += delta
		if fly_time > .5:
			fly_time = 0
			if is_instance_valid(attacker):
				if global_position.y > attacker.global_position.y + 30:
					gravity = rand_range(-.1,0)
				elif global_position.y < attacker.global_position.y - 30:
					gravity = rand_range(0,.1)
			else:
				gravity = 0

func check_last_seen(delta):
	if last_seen > 1:
		last_seen = 0
		choose_direction()
	else:
		last_seen += delta
#check if player is near by
func check_player_near(delta):
	if last_seen > check_player_near_time/Global.difficulty:
		last_seen = 0
		var players = get_tree().get_nodes_in_group("players")
		for player in players:
			if global_position.distance_to(player.global_position) < 200:
				if !player.dead:
					attacker = player
					activate()
					if weapon == "headfire":
						shoot_headfire()
	else:
		last_seen += delta

func animations():
	if dead:
		$Sprite.play("death")
	elif hit_animation:
		$Sprite.play("hit")
	elif attack_animation:
		$Sprite.play("attack")
	elif walk_animation:
		$Sprite.play("walk")
	else:
		$Sprite.play("default")

func play_act_snd(delta):
	if snd_act != "":
		if last_snd > rand_range(3,6):
			Sounds.play_sound(self,snd_act)
			last_snd = 0
		else:
			last_snd += delta

func choose_direction():
	if !dead && !attacking:
		var px = rand_range(0,100)
		if px < 90:
			face_position(attacker.global_position.x)
		else:
			face_position(-attacker.global_position.x)

func face_position(pos):
	if !dead && !attacking:
		if global_position.x  > pos + 200:
			#move left
			direction = -1
			$Sprite.flip_h = false
		elif global_position.x < pos - 200:
			#move right
			$Sprite.flip_h = true
			direction = 1

func face_attacker():
	if is_instance_valid(attacker) && !dead:
		var pos = attacker.global_position.x
		if pos - global_position.x  <  0:
			#move left
			direction = -1
			$Sprite.flip_h = false
		elif pos - global_position.x > 0:
			#move right
			$Sprite.flip_h = true
			direction = 1
	
func walk(delta):
	if active:
		if velocity.x > 0:
			walk_animation = true
			$Sprite.flip_h = true
		elif velocity.x < 0:
			walk_animation = true
			$Sprite.flip_h = false
		else:
			walk_animation = false
			#turn(delta)
			
		if !attacking:
			velocity.x = speed * delta * direction * 10
		

func attack():
	if is_instance_valid(attacker) && !attacking:
		attacking = true
		
		#attack delay
		var delay = attack_delay / Global.difficulty
		yield(get_tree().create_timer(delay),"timeout")
		face_attacker()
		if weapon == "pistol":
			yield(get_tree().create_timer(rand_range(0.0,1.0)),"timeout")
			velocity.x = 0
			shoot_pistol()
			yield(get_tree().create_timer(.2),"timeout")
			attacking = false
		elif weapon == "chaingun":
			yield(get_tree().create_timer(rand_range(0.0,1.0)),"timeout")
			velocity.x = 0
			shoot_chaingun()
		elif weapon == "shotgun":
			yield(get_tree().create_timer(rand_range(0.0,1.0)),"timeout")
			velocity.x = 0
			shoot_shotgun()
			yield(get_tree().create_timer(.2),"timeout")
			attacking = false
		elif weapon == "fireball":
			shoot_fireball()
			yield(get_tree().create_timer(.2),"timeout")
			attacking = false
		elif weapon == "headfire":
			shoot_headfire()
			yield(get_tree().create_timer(.2),"timeout")
			attacking = false
		elif weapon == "heat_missile":
			shoot_heat_missile()
			yield(get_tree().create_timer(.2),"timeout")
			attacking = false
		elif weapon == "plasma":
			shoot_plasma()
			yield(get_tree().create_timer(2),"timeout")
			attacking = false
		elif weapon == "melee":
			yield(get_tree().create_timer(1),"timeout")
			attacking = false
			
		attack_animation = false
		
func shoot_pistol():
	if !dead:
		face_attacker()
		Sounds.play_sound(self,"pistol")
		attack_animation = true
		$Sprite.frame = 0
		puff()
		
func shoot_chaingun():
	if !dead:
		var shots = Global.difficulty*5
		for i in range(shots):
			if dead || hit:
				attacking = false
				attack_animation = false
				break
			Sounds.play_sound(self,"pistol")
			attack_animation = true
			$Sprite.frame = 0
			puff()
			var delay = .2
			if Global.difficulty > 2:
				if attacker != null:
					face_attacker()
				delay = .1
			yield(get_tree().create_timer(delay),"timeout")
			if i == shots-1:
				attacking = false
				attack_animation = false

		
func shoot_shotgun():
	if !dead:
		attack_animation = true
		face_attacker()
		$Sprite.frame = 0
		Sounds.play_sound(self,"shotgun")
		for i in range(0,4):
			puff()

func shoot_fireball():
	if !dead:
		face_attacker()
		attack_animation = true
		var direction = get_direction()
		var pos = $projectile.global_position
		if direction == 1:
			pos = $projectile2.global_position
		Global.create_fireball(self,pos,direction)
		
func shoot_heat_missile():
	if !dead:
		face_attacker()
		attack_animation = true
		var direction = get_direction()
		var pos = $projectile.global_position
		if direction == 1:
			pos = $projectile2.global_position
		Global.create_projectile(self,pos,direction,"res://objects/heat_missile.tscn")
		
func shoot_headfire():
	if !dead:
		face_attacker()
		attack_animation = true
		var direction = get_direction()
		var pos = $projectile.global_position
		if direction == 1:
			pos = $projectile2.global_position
		Global.create_headfire(self,pos,direction)

func shoot_plasma():
	if !dead:
		face_attacker()
		var direction = get_direction()
		var pos = $projectile.global_position
		if direction == 1:
			pos = $projectile2.global_position
		Global.create_plasma_shot(self,pos,direction)

func get_direction():
	if $Sprite.flip_h:
		return 1
	else:
		return -1

func puff():
	var space_state = get_world_2d().direct_space_state
	# use global coordinates, not local to node
	var tar_distance = 1000 * direction
		
	var target = Vector2(global_position.x + tar_distance, global_position.y + rand_range(-100,100))
	var pos = Vector2(global_position.x,global_position.y - 5)
	var result = space_state.intersect_ray(pos, target, [self])
	if result:
		pass
		Global.create_puff(result.position,self)

func check_wall():
	var space_state = get_world_2d().direct_space_state
	# use global coordinates, not local to node
	var tar_distance = 50 * direction
		
	var target = Vector2(global_position.x + tar_distance, global_position.y)
	var result = space_state.intersect_ray(global_position, target, [self],1)
	if result:
		turn()


func hit_scanner():
	#set_direction()
	
	var space_state = get_world_2d().direct_space_state
	# use global coordinates, not local to node
	var tar_distance = 1000
	if $Sprite.flip_h:
		tar_distance = 1000
	else:
		tar_distance = -1000
		
	var target = Vector2(global_position.x + tar_distance, global_position.y + rand_range(-100,100))
	var pos = Vector2(global_position.x,global_position.y - 10)
	var result = space_state.intersect_ray(pos, target, [self])
	if result:
		if result.collider.is_in_group("players"):
				attacker = result.collider
				attack = true
				if !active && !attacker.dead:
					activate()
				
		if result.collider == attacker:
			last_seen = 0
			if !attacking:
				if attack:
					attack = false
					if rand_range(0,100) > 75:
						attack()
					else:
						pass
						#follow_attacker(delta)
				else:
					attack = true
				

func take_damage(creator,damage):
	if !dead:
		life -= damage
		hit = true
		attacker = creator
		activate()
		hit_animation = true
		if !playing_snd:
			var snd = snd_pain[randi() % snd_pain.size()]
			playing_snd = true
			Sounds.play_sound(self,snd)
			yield(get_tree().create_timer(.2),"timeout")
			hit_animation = false
			yield(get_tree().create_timer(.2),"timeout")
			attack()
			playing_snd = false
			hit_animation = false
			hit = false
			if pause_on_hit:
				velocity.x = 0


func activate():
	if !dead && !active:
		face_position(attacker.global_position.x)
		active = true
		var snd = snd_site[randi() % snd_site.size()]
		if site_sound:
			Sounds.play_sound(self,snd)

func drop_item():
	# Instance as child:
	if drop_item != "":
		var item = load(drop_item).instance()
		get_tree().current_scene.add_child(item)
		item.position = global_position

func wall():
	pass
	#direction *= -1

func player_siting(body):
	if !active && body.is_in_group("players"):
		attacker = body
		activate()

	if active && !dead && body == attacker:
		if attacker.dead:
			attack = false
		else:
			attack = true


func turn():
	if !dead:
		turn_delay = turn_delay_set
		direction *= -1
		velocity.x *= -1
		
func death():
	gravity = 1
	Global.enemies_killed += 1
	velocity.x = 0
	dead = true
	active = false
	#remvoe from enemy layer 
	set_collision_layer_bit(2,false)

	death_body.disabled = false
	time_out = 0
	body.disabled = true
	
	var snd = snd_death[randi() % snd_death.size()]
	Sounds.play_sound(self,snd)
	drop_item()
	
	if free_on_death:
		yield(get_tree().create_timer(2),"timeout")
		queue_free()
	
func respawn():
	if dead:
		Global.enemies_killed -= 1
		dead = false
		life = start_life
		set_collision_layer_bit(2,true)
		death_body.disabled = true
		body.disabled = false

func _on_onscreen_screen_entered():
	onscreen = true
	if Global.difficulty >= 6:
		respawn()


func _on_onscreen_screen_exited():
	time_out = 0
	#set offscreen time to 5 seconds
	#if off screen for more than 5 seconds become inactive
	offscreen = 5
	onscreen = false

func melee_attack(body):
	velocity.x = 0
	Sounds.play_sound(self,snd_melee[0])
	attack_animation = true
	$Sprite.frame = 0
	body.take_damage(self,melee_damage)
	yield(get_tree().create_timer(1),"timeout")
	attack_animation = false

func _on_Area2D_body_entered(body):
	if !dead:
		if body.is_in_group("players"):
			if rand_range(0,2) < 1:
				melee_attack(body)
		else:
			if rand_range(0,5) < 1:
				melee_attack(body)
