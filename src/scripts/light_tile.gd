extends TileMap

export (bool) var flicker = true
export (bool) var fade = false
export (bool) var random_brightness = false

export (float) var max_brightness = 1
export (float) var min_brightness = 0

#flicker settings
var light_on = true
var time_passed = 0

export (float) var on_time_min = .2
export (float) var on_time_max = 1
export (float) var off_time_min = .2
export (float) var off_time_max = .5

#fade settings
export (float) var fade_time = 1

var tween = Tween.new()

func _ready():
	add_child(tween)
	tween.connect("tween_completed", self, "_on_tween_completed")
	
	if fade:
		fade()
	pass
	
func _process(delta):
	
	if !fade:
		if check_time(delta):
			if flicker:
				flicker()
	
func check_time(delta):
	time_passed += delta
	var time = 0
	if !light_on:
		time = rand_range(on_time_min,on_time_max)
		
	else:
		time = rand_range(off_time_min,off_time_max)
	
	if time_passed > time:
		time_passed = 0
		return true
	else:
		return false
		
func flicker():
	if !light_on:
		light_on = true
		modulate = Color(1,1,1,max_brightness)
	else:
		light_on = false
		modulate = Color(1,1,1,min_brightness)
		
func fade():
	if !light_on:
		light_on = true
		tween.interpolate_property(self, "modulate", modulate, Color(1, 1, 1, max_brightness), fade_time , Tween.EASE_IN_OUT, Tween.EASE_IN_OUT)
	else:
		light_on = false
		tween.interpolate_property(self, "modulate", modulate, Color(1, 1, 1, min_brightness), fade_time , Tween.EASE_IN_OUT, Tween.EASE_IN_OUT)
	
	tween.start()
	
func _on_tween_completed(object,key):
	fade()
