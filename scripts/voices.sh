#!/bin/bash
###################################################################### 
#Copyright (C) 2021  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

output="../src/res/voices/"

rm -fr "$output"

mkdir -p "$output"

cat voice.lst|while read l;
do
  file="$(echo "$l"|cut -d\| -f1)"
  file="${output}${file}"
  text="$(echo "$l"|cut -d\| -f2)"
  #echo "$text" | text2wave | oggenc - -o "${file}.ogg"
  echo "$text" | text2wave -o "${file}.wav"
  cat "${file}.wav"|oggenc - -o "${file}.ogg"
  #ffmpeg -i "$file.wav" "$file.ogg" 2> /dev/null
  rm "${file}.wav" 
  echo "${file}.ogg"
done
